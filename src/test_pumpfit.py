import pumpfit
import unittest
import numpy as np

class TestGeneratingPoints(unittest.TestCase):
        def test_generating_points_size(self):
            Q = np.linspace(0,3,7)
            H = np.ones((9,1))
            H = -np.square(Q)+11
            E = np.linspace(0.5,0.9,7)
            P=[]
            N = 10.0
            test_pump=pumpfit.Pump(Q, H,P,E,N)
            H3, Q3, E3, N3, P3 =test_pump._generating_points()
            self.assertEqual(np.shape(H3)[0], np.shape(test_pump.H)[0])

        #Test if the efficiency does not change
        def test_generating_points_efficiency(self):
            Q = np.linspace(0,3,7)
            H = np.ones((9,1))
            H = -np.square(Q)+11
            E = np.linspace(0.5,0.9,7)
            P=[]
            N = 10.0
            test_pump=pumpfit.Pump(Q, H,P,E,N)
            H3, Q3, E3, N3, P3 =test_pump._generating_points()
            self.assertEqual(E3[0,1] , E3[0,2])


        #Check if Q changes proportionally
        def test_generating_points_discharge(self):
            Q = np.linspace(0,3,7)
            H = np.ones((9,1))
            H = -np.square(Q)+11
            E = np.linspace(0.5,0.9,7)
            P=[]
            N = 10.0
            test_pump=pumpfit.Pump(Q, H,P,E,N)
            H3, Q3, E3, N3, P3 =test_pump._generating_points()
            self.assertEqual(Q3[1,1]/Q3[2,1],Q3[1,2]/Q3[2,2])

class RemovingEfficiency(unittest.TestCase):
        def test_removing_losses(self):
            Q = np.linspace(0,3,7)
            H = np.ones((9,1))
            H = -np.square(Q)+11
            E = np.linspace(0.5,0.9,7)
            P=[]
            N = 10.0
            H_original = H
            test_pump=pumpfit.Pump(Q, H,P,E,N)
            test_pump.losshref = 1
            test_pump.lossqref = 3
            test_pump._removing_losses()
            self.assertEqual(H_original[1] , H[1])

        def test_restrict_points_efficiency(self):
            Q = np.linspace(0,3,7)
            H = np.ones((9,1))
            H = -np.square(Q)+11
            E = np.linspace(0.5,0.9,7)
            P=[]
            N = 10.0
            H_original = H
            test_pump=pumpfit.Pump(Q, H,P,E,N)
            H3, Q3, E3, N3, P3 = test_pump._generating_points()
            restricted_area = test_pump._restrict_points_efficiency(H3, Q3, E3, P3, N3, test_pump.minimum_efficiency)
            ismember = set(Q3.flatten()).issuperset(set(restricted_area['Q'].flatten()))
            self.assertTrue(ismember)

        def test_extra_area_calc(self):
            Q = np.linspace(0,3,7)
            H = -np.square(Q)+11
            E = np.linspace(0.2,0.9,7)
            P=[]
            N = 10.0

            test_pump=pumpfit.Pump(Q, H,P,E,N)
            test_pump.losshref = 1
            test_pump.lossqref = 3
            test_pump._removing_losses()
            H3, Q3, E3, N3, P3 = test_pump._generating_points()
            restricted_area = test_pump._restrict_points_efficiency(H3, Q3, E3, P3, N3, test_pump.minimum_efficiency)
            test_pump.extra_line_coef = np.reshape(np.array([0, 0, 0, -1, 2, 0, 0]),(1,7))
            test_pump.point_coordinates = [1.0, 4.0]
            extra_line_coef_calc =  test_pump._extra_area_calc()
            self.assertEqual(extra_line_coef_calc[0,6],0)

        def test__restrict_points_for_working_area(self):
            Q = np.linspace(0,3,7)
            H = -np.square(Q)+11
            E = np.linspace(0.2,0.9,7)
            P=[]
            N = 10.0
            test_pump=pumpfit.Pump(Q, H,P,E,N)
            test_pump.losshref = 1
            test_pump.lossqref = 3
            test_pump._removing_losses()
            H3, Q3, E3, N3, P3 = test_pump._generating_points()
            restricted_area = test_pump._restrict_points_efficiency(H3, Q3, E3, P3, N3, test_pump.minimum_efficiency)
            test_pump.extra_line_coef = np.reshape(np.array([0, 0, 0, -1, 2, 0, 0]),(1,7))
            test_pump.point_coordinates = [1.0, 4.0]
            extra_line_coef_calc =  test_pump._extra_area_calc()
            Q4, H4, E4, P4, N4 = test_pump._restrict_points_for_working_area(restricted_area['H'], restricted_area['Q'], restricted_area['P'], restricted_area['E'], restricted_area['N'], extra_line_coef_calc)
            self.assertTrue(np.size(Q4)<=np.size(Q3))

if __name__ == '__main__':
    unittest.main()#