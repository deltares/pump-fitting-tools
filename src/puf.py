import pumpfit
import unittest
import numpy as np
import matplotlib.pyplot as plt


Q = np.linspace(0,3,7)
H = -np.square(Q)+11
E = np.linspace(0.2,0.9,7)
P=[]
N =9

test_pump=pumpfit.Pump(Q, H,P,E,N)
test_pump.losshref = 1
test_pump.lossqref = 3
test_pump._removing_losses()
H3, Q3, E3, N3, P3 = test_pump._generating_points()

restricted_area = test_pump._restrict_points_efficiency(H3, Q3, E3, P3, N3, test_pump.minimum_efficiency)

print(Q3)
print((restricted_area['Q']))

plt.plot(Q3,H3, 'r*')
plt.plot(restricted_area['Q'], restricted_area['H'], 'b^')

print(test_pump.extra_line_coef)
print(np.shape(test_pump.extra_line_coef))
#h=2
test_pump.extra_line_coef = np.reshape(np.array([0, 0, 0, -1, 2, 0, 0]),(1,7))
test_pump.point_coordinates = [1.0, 4.0]
print(test_pump.extra_line_coef)
print(np.shape(test_pump.extra_line_coef))

extra_line_coef_calc =  test_pump._extra_area_calc()

Q, H, E, P, N = test_pump._restrict_points_for_working_area(restricted_area['H'], restricted_area['Q'], restricted_area['P'], restricted_area['E'], restricted_area['N'], extra_line_coef_calc)
plt.plot(Q,H, 'y+')


#plt.show()


Q = np.linspace(0,3,7)

H = -np.square(Q)+11
E = np.linspace(0.2,0.9,7)
P=[]
N = 10.0
test_pump=pumpfit.Pump(Q, H,P,E,N)
test_pump.losshref = 1
test_pump.lossqref = 3
test_pump._removing_losses()
H3, Q3, E3, N3, P3 = test_pump._generating_points()
restricted_area = test_pump._restrict_points_efficiency(H3, Q3, E3, P3, N3, test_pump.minimum_efficiency)
test_pump.extra_line_coef = np.reshape(np.array([0, 0, 0, -1, 2, 0, 0]),(1,7))
test_pump.point_coordinates = [1.0, 4.0]
extra_line_coef_calc =  test_pump._extra_area_calc()
Q4, H4, E4, P4, N4 = test_pump._restrict_points_for_working_area(restricted_area['H'], restricted_area['Q'], restricted_area['P'], restricted_area['E'], restricted_area['N'], extra_line_coef_calc)


bottom_polynom_coef, convexity_parameter = test_pump._approximating_polynoms(restricted_area['bottom_rpm_line_h'], restricted_area['bottom_rpm_line_q'], 1, constant_speed=0)
top_polynom_coef, convexity_parameter = test_pump._approximating_polynoms(restricted_area['top_rpm_line_h'], restricted_area['top_rpm_line_q'], -1, constant_speed=0)
right_polynom_coef, convexity_parameter = test_pump._approximating_polynoms(restricted_area['right_eff_line_h'], restricted_area['right_eff_line_q'], 1, constant_speed=0)
left_polynom_coef, convexity_parameter = test_pump._approximating_polynoms(restricted_area['left_eff_line_h'], restricted_area['left_eff_line_q'], -1, constant_speed=0)
convex_polynom_coef_array, convexity_parameter_array = test_pump._approximate_convex_extra_lines(extra_line_coef_calc, Q4)
working_area_coef = test_pump._working_area_calc(bottom_polynom_coef, top_polynom_coef, left_polynom_coef, right_polynom_coef,
                                           test_pump.point_coordinates, test_pump.pump_type, convex_polynom_coef_array, convexity_parameter_array)

test_pump._approximate_linear_working_area_boundaries(restricted_area, test_pump.point_coordinates, extra_line_coef_calc, Q4, test_pump.pump_type)