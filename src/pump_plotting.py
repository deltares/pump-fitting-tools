'''
This code is reading a csv file with pump data and makes a convex fit.
csv_reader: if one of them (E or P ) is missing, it is calculated
Removing losses: if loss coef is given, E is recalculated
Generating points: in case of Ackeret correction the power is calculated again
If it is inconsistent in a row, calculated efficiency is used.
'''

from sys import path
#path.append("D:\Casadi")
#! =====================
from casadi import *
import casadi as ca
from numpy import *
import scipy
import csv
import sys
import logging
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
logger = logging.getLogger("pump-fitting-tools")
logging.basicConfig(level=logging.INFO)



def _plotting_gradient(H, Q, P, power_calculated):
    # Creating a grid, h and q defines grid distance
    # This also gives the number of gridpoints
    # Note that these grid distances might have to be set differently for every plot
    # in order to see very well the change of the gradient in both directions
    # it depends basically on the proportion of these two numbersS
    h_dist = 0.5
    q_dist = 0.05
    step_number = 20.0

    grid_x, grid_y = np.mgrid[min(H):max(H):(max(H)-min(H))/step_number, min(Q):max(Q):(max(Q)-min(Q))/step_number]

    # Creates arrays from the list objects (row arrays)
    H2 = np.array(H)
    P2 = np.array(P)
    Q2 = np.array(Q)

    # Creates a datalength by 2 array, with columns H and Q
    QH2 = np.vstack((H2, Q2))

    # Interpolates the P data for a grid. Method can be nearest or linear as well.
    grid_z2 = griddata(np.transpose(QH2), np.transpose(np.array(P)), (grid_x, grid_y), method='cubic')

    plt.subplot(211)
    # Initialize an array for the gradient
    e, f = grid_z2.shape
    grad_q = np.zeros((e, f))
    grad_h = np.zeros((e, f))

    # It calculates the gradient with finite difference approximation
    for row in range(1, e-1):
        for col in range(1, f-1):
            grad_h[row, col] = (grid_z2[row+1, col]-grid_z2[row-1, col])/(2*h_dist)
            grad_q[row, col] = (grid_z2[row, col+1]-grid_z2[row, col-1])/(2*q_dist)

    plt.quiver(grid_y, grid_x, grad_q, grad_h)
    plt.ylabel('Head (m)')
    plt.title("Original and modelled power gradient")

    # Create a grid and calculate the gradient
    grid_z3 = griddata(np.transpose(QH2), np.transpose(np.array(power_calculated)), (grid_x, grid_y), method='cubic')
    for row in range(1, e-1):
        for col in range(1, f-1):
            grad_h[row, col] = (grid_z3[row+1, col]-grid_z3[row-1, col])/(2*h_dist)
            grad_q[row, col] = (grid_z3[row, col+1]-grid_z3[row, col-1])/(2*q_dist)

    plt.subplot(212)
    plt.quiver(grid_y, grid_x, grad_q, grad_h)
    plt.ylabel('Head (m)')
    plt.xlabel('Flow [$m^3\,s^{-1}$]')

    plt.savefig('Gradients.png', bbox_inches='tight', pad_inches=0.1)

def _level_curves(H6, Q6, P6, file_name='LevelCurves'):

    H=_2darray_to_np_flattened(H6)
    Q=_2darray_to_np_flattened(Q6)
    P=_2darray_to_np_flattened(P6)

    # This number defines the densness of the grid. If the grid is too dense,
    # the level curves will look like wobbeling (too curvy)
    step_number = 120.0

    # Creates a datalength by 2 array, with columns H and Q
    QH2 = np.vstack((H, Q))

    grid_x2, grid_y2 = np.mgrid[min(H):max(H):(max(H)-min(H))/step_number, min(Q):max(Q):(max(Q)-min(Q))/step_number]
    grid_z4 = griddata(np.transpose(QH2), np.transpose(np.array(P)), (grid_x2, grid_y2), method='cubic')
    levels = np.around(np.arange(min(P), max(P), (max(P)-min(P))/10), -3)

    fig = plt.figure(figsize=plt.figaspect(1.))
    #CS = plt.contourf(grid_y2, grid_x2, grid_z4, levels, cmap=cm.viridis)
    #CSC = plt.contour(grid_y2, grid_x2, grid_z4, levels, colors='k')
    CS = plt.contourf(grid_y2, grid_x2, grid_z4, cmap=cm.viridis)
    CSC = plt.contour(grid_y2, grid_x2, grid_z4, colors='k')
    plt.colorbar(CS, shrink=0.8, extend='both')
    #    plt.clabel(CSC, levels[1::2],  # label every second level
    #           inline=1,
    #           fmt='%1.1f',
    #           fontsize=14)
    plt.clabel(CSC)
    plt.xlabel('Q (m^3/s)')
    plt.ylabel('Head [m]')
    plt.title(file_name)
    plt.savefig( file_name + '.png', bbox_inches='tight', pad_inches=0.1)

def _plotting_curves(Q,H, fig_name):
    plt.clf()
    fig = plt.figure
    plt.plot(Q,H)
    plt.xlabel('Q (m^3/s)')
    plt.ylabel('Head [m]')
    plt.savefig(fig_name + '.png', bbox_inches='tight', pad_inches=0.1)

def _plotting_losses(Q, Hstat, Hman):
    plt.clf()
    fig = plt.figure
    plt.plot(Q,Hstat, '-b', label = 'Static head')
    plt.plot(Q, Hman, '-r', label = 'Manomentric head')
    plt.plot(Q, -Hman+Hstat, '-g', label = 'Losses')
    plt.legend()
    plt.xlabel(r'Q $[ m^3/s]$')
    plt.ylabel('Head [m]')
    plt.title('Pump curves with losses')
    #plt.autoscale(enable=True, axis='x', tight=True)
    fig_name= 'Losses'
    plt.savefig(fig_name + '.png', bbox_inches='tight', pad_inches=0.1)

def _plotting_power(H, Q, P, power_calculated, error_calculated):
    marker_size = 60
    # Two subplots, the axes array is 1-d
    f, axarr = plt.subplots(3, sharex=True)
    c1 = axarr[0].scatter(Q, H, c=P, s=marker_size, lw=0)
    cbar = plt.colorbar(c1, ax=axarr[0])

    c2 = axarr[1].scatter(Q, H, c=power_calculated, s=marker_size, lw=0)
    c3 = axarr[2].scatter(Q, H, c=error_calculated, s=marker_size, lw=0)

    axarr[0].set_title('Original power')
    axarr[1].set_title('Power calculated')
    axarr[2].set_title('Error')

    plt.xlabel('Q (m^3/s)')
    plt.ylabel('Head [m]')
    #axarr[0].set_ylim(0.2, 0.65)
    #axarr[1].set_ylim(0.2, 0.65)

    cbar = plt.colorbar(c2, ax=axarr[1])
    cbar = plt.colorbar(c3, ax=axarr[2])


    f = plt.gcf()
    f.set_size_inches(7, 7)
    plt.savefig('PowersAndError.png', bbox_inches='tight', pad_inches=0.1)

    #fig2 = plt.figure(20)
    #plt.scatter(Q, H, c=power_calculated, s=marker_size, lw=0)
    #plt.savefig('Powers2.png', bbox_inches='tight', pad_inches=0.1)
    #plt.colorbar
def _plotting_points(H6, Q6, P6, plot_name='points'):

    H=_2darray_to_np_flattened(H6)
    Q=_2darray_to_np_flattened(Q6)
    P=_2darray_to_np_flattened(P6)

    marker_size = 60
    c1 = plt.scatter(Q, H,  c=P, s=marker_size, lw=0)
    cbar = plt.colorbar(c1)
    plt.title(plot_name)
    plt.xlabel('Q (m^3/s)')
    plt.ylabel('Head [m]')
    f = plt.gcf()
    f.set_size_inches(5, 5)
    plt.savefig(plot_name + '.png', bbox_inches='tight', pad_inches=0.1)

def _plotting_points2(H6, Q6, P6, plot_name='points'):

    H=_2darray_to_np_flattened(H6)
    Q=_2darray_to_np_flattened(Q6)
    P=_2darray_to_np_flattened(P6)

    marker_size = 35
    fig = plt.figure()
    ax = Axes3D(fig)
    #ax = plt.subplots(111, projection='3d')
    ax.scatter(Q, H, P,  c=P, lw=0, s=marker_size)

    #for i, txt in enumerate(P):
    #    ax.annotate(str(P[i]), (Q[i],H[i]))
    #c1 = plt.scatter(Q, H,  c=P, s=marker_size, lw=0)
    #cbar = plt.colorbar(c1)
    plt.title(plot_name)
    plt.xlabel('Q (m^3/s)')
    plt.ylabel('Head [m]')
    f = plt.gcf()
    f.set_size_inches(5, 5)
    plt.savefig(plot_name + '.png', bbox_inches='tight', pad_inches=0.1)
def _plotting_level_curve_surface(H, Q, P, power_calculated):
    # This number defines the densness of the grid. If the grid is too dense,
    # the level curves will look like wobbeling (too curvy)
    step_number = 15.0

    # Creates arrays from the list objects (row arrays)
    H2 = np.array(H)
    P2 = np.array(P)
    Q2 = np.array(Q)

    # Creates a datalength by 2 array, with columns H and Q
    QH2 = np.vstack((H2, Q2))


    #-----------------------------------------------------------------------------------

    grid_x2, grid_y2 = np.mgrid[min(H):max(H):(max(H)-min(H))/step_number, min(Q):max(Q):(max(Q)-min(Q))/step_number]
    #grid_z4 = griddata(np.transpose(QH2), np.transpose(np.array(P)), (grid_x2, grid_y2), method='cubic')
    grid_z4 = griddata(np.transpose(QH2), np.transpose(np.array(P)), (grid_x2, grid_y2), method='cubic')
    grid_z5 = griddata(np.transpose(QH2), np.transpose(np.array(power_calculated)), (grid_x2, grid_y2), method='cubic')

    fig = plt.figure(figsize=plt.figaspect(1.))
    levels = np.around(np.arange(min(P2), max(P2), (max(P2)-min(P2))/10), -3)

    ax = fig.add_subplot(2, 1, 1)
    ax.set_title('Level curves for power')
    #ax.set_xlabel('Flow [$m^3\,s^{-1}$]')
    ax.set_ylabel('Head [m]')

    #CS = ax.contourf(grid_y2, grid_x2, grid_z4, levels, cmap=cm.viridis)
    #CSC = ax.contour(grid_y2, grid_x2, grid_z4, levels, colors='k')
    CS = ax.contourf(grid_y2, grid_x2, grid_z4, cmap=cm.viridis)
    CSC = ax.contour(grid_y2, grid_x2, grid_z4, colors='k')
    plt.colorbar(CS, shrink=0.8, extend='both', ax=ax)
    #plt.clabel(CSC, levels[1::2],  # label every second level
    #           inline=1,
    #           fmt='%1.1f',
    #           fontsize=14)
    plt.clabel(CSC)


    ax = fig.add_subplot(2, 1, 2)
    #CS = ax.contourf(grid_y2, grid_x2, grid_z5, levels, cmap=cm.viridis)
    #CSC = ax.contour(grid_y2, grid_x2, grid_z5, levels, colors='k')
    CS = ax.contourf(grid_y2, grid_x2, grid_z5, cmap=cm.viridis)
    CSC = ax.contour(grid_y2, grid_x2, grid_z5, colors='k')
    ax.set_title('Power approximation')
    ax.set_ylabel('Head [m]')
    ax.set_xlabel('Flow [$m^3\,s^{-1}$]')
    plt.colorbar(CS, shrink=0.8, extend='both', ax=ax)
    #plt.clabel(CSC, levels[1::2],  # label every second level
    #           inline=1,
    #           fmt='%1.1f',
    #           fontsize=14)
    plt.clabel(CSC)

    plt.savefig('LevCurvesApprox.png', bbox_inches='tight', pad_inches=0.1)

    #-------------------------------------

    fig = plt.figure(figsize=plt.figaspect(1.))

    ax = fig.add_subplot(2, 1, 1, projection='3d')
    ax.set_title('Power')
    surf = ax.plot_surface(grid_y2, grid_x2, grid_z4, cmap=cm.plasma, rstride=1, cstride=1, linewidth=0,
                           vmin=min(np.nanmin(grid_z4), np.nanmin(grid_z5)), vmax=max(np.nanmax(grid_z4), np.nanmax(grid_z5)))
    cbar = plt.colorbar(surf, ax=ax)

    ax = fig.add_subplot(2, 1, 2, projection='3d')
    surf = ax.plot_surface(grid_y2, grid_x2, grid_z5, cmap=cm.plasma, rstride=1, cstride=1, linewidth=0,
                           vmin=min(np.nanmin(grid_z4), np.nanmin(grid_z5)), vmax=max(np.nanmax(grid_z4), np.nanmax(grid_z5)))
    ax.set_title('Power approximation')
    cbar = plt.colorbar(surf, ax=ax)

    plt.savefig('Surface.png', bbox_inches='tight', pad_inches=0.1)


def _plotting_working_area(Q3, H3, E3,
                           bottom_rpm_line_q, bottom_rpm_line_h, top_rpm_line_q, top_rpm_line_h, left_eff_line_q, left_eff_line_h, right_eff_line_q, right_eff_line_h,
                           bottom_polynom_coef, top_polynom_coef, left_polynom_coef, right_polynom_coef,  working_area_coef, Q, H, E, point_coordinates, savename='WorkingArea'):
    '''
    The rpm line are ther original
    '''
    plt.clf()
    fig = plt.figure()
    ax = plt.subplot(111)

    evaluation_points=10
    orange_color = '#ff7f0e'
    gray_color = '#989898'

    h_plot_values=np.linspace(np.amin(H3), np.amax(H3),evaluation_points)
    q_plot_values=np.linspace(np.amin(Q3), np.amax(Q3),evaluation_points)
    #CS = ax.scatter(Q3, H3, c=E3, s=10, lw=0, cmap = 'PuRd')
    CS = ax.scatter(Q3, H3, c=E3, s=20, lw=0, cmap = 'gray')

    #plt.colorbar(CS, shrink=0.8, extend='both')
    cbar = plt.colorbar(CS, shrink=0.8)
    cbar.set_label('efficiency', color = gray_color)
    cbar.outline.set_visible(False)



    #for k in range(working_area_coef.shape[0]):
    #    if abs(working_area_coef[k,5])<1e-7:
    #        #Then it is given in Q
    #        ax.plot(q_plot_values,working_area_coef[k,0]*np.square(q_plot_values)+working_area_coef[k,1]*q_plot_values+np.ones(evaluation_points)*working_area_coef[k,4],'k')
    #    else:
    #        ax.plot(working_area_coef[k,2]*np.square(h_plot_values)+working_area_coef[k,3]*h_plot_values+np.ones(evaluation_points)*working_area_coef[k,4],h_plot_values,'k')

    ax.plot(bottom_rpm_line_q, bottom_rpm_line_h, ls = '--', color = orange_color)
    ax.plot(top_rpm_line_q, top_rpm_line_h, ls = '--', color = orange_color)
    ax.plot(left_eff_line_q, left_eff_line_h, ls = '--', color = orange_color)
    ax.plot(right_eff_line_q, right_eff_line_h, ls = '--', color = orange_color)


    # Hide the right and top spines
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)

    ax.spines['bottom'].set_color(gray_color)
    ax.spines['left'].set_color(gray_color)

    ax.tick_params(axis='x', colors=gray_color)
    ax.tick_params(axis='y', colors=gray_color)

    ax.yaxis.label.set_color(gray_color)


    # Only show ticks on the left and bottom spines
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')

    plt.xlim(np.amin(Q3), np.amax(Q3)*1.1)
    plt.ylim(np.amin(H3), np.amax(H3)*1.1)

    plt.ylabel('Head [m]', color = gray_color)
    plt.xlabel('Flow [$m^3\,s^{-1}$]', color = gray_color)
    #plt.title("Efficiency plot with chosen limits")

    plt.savefig(savename+ '.png', bbox_inches='tight', pad_inches=0.1)
#---------------------------------------------------------------------------

    plt.clf()
    fig = plt.figure()
    ax = plt.subplot(111)

    evaluation_points=10
    orange_color = '#ff7f0e'
    gray_color = '#989898'

    h_plot_values=np.linspace(np.amin(H3), np.amax(H3),evaluation_points)
    q_plot_values=np.linspace(np.amin(Q3), np.amax(Q3),evaluation_points)
    #CS = ax.scatter(Q, H, c=E, s=20, lw=0, cmap = 'gray')
    #CS = ax.scatter(Q, H, c=gray_color, marker = 's', s=150, lw=0)

    #cbar = plt.colorbar(CS, shrink=0.8)
    #cbar.set_label('efficiency', color = gray_color)
    #cbar.outline.set_visible(False)
    ax.plot(point_coordinates[0], point_coordinates[1], marker='s', color = 'r')


    for k in range(working_area_coef.shape[0]):
        if abs(working_area_coef[k,5])<1e-7:
            #Then it is given in Q
            ax.plot(q_plot_values,working_area_coef[k,0]*np.square(q_plot_values)+working_area_coef[k,1]*q_plot_values+np.ones(evaluation_points)*working_area_coef[k,4],'k')
        else:
            ax.plot(working_area_coef[k,2]*np.square(h_plot_values)+working_area_coef[k,3]*h_plot_values+np.ones(evaluation_points)*working_area_coef[k,4],h_plot_values,'k')

    ax.plot(bottom_rpm_line_q, bottom_rpm_line_h, ls = '--', color = orange_color)
    ax.plot(top_rpm_line_q, top_rpm_line_h, ls = '--', color = orange_color)
    ax.plot(left_eff_line_q, left_eff_line_h, ls = '--', color = orange_color)
    ax.plot(right_eff_line_q, right_eff_line_h, ls = '--', color = orange_color)


    # Hide the right and top spines
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)

    ax.spines['bottom'].set_color(gray_color)
    ax.spines['left'].set_color(gray_color)

    ax.tick_params(axis='x', colors=gray_color)
    ax.tick_params(axis='y', colors=gray_color)

    ax.yaxis.label.set_color(gray_color)


    # Only show ticks on the left and bottom spines
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')

    plt.xlim(np.amin(Q3), np.amax(Q3)*1.1)
    plt.ylim(np.amin(H3), np.amax(H3)*1.1)

    plt.ylabel('Head [m]', color = gray_color)
    plt.xlabel('Flow [$m^3\,s^{-1}$]', color = gray_color)
    #plt.title("Efficiency plot with chosen limits")

    plt.savefig(savename+'_boundary' '.png', bbox_inches='tight', pad_inches=0.1)


def _plotting_working_area_boundary2(Q, H, Q_bnd, H_bnd, coef, point_coordinates=[0,0]):
    plt.clf()
    CS = plt.scatter(Q, H)
    CS = plt.scatter(Q_bnd, H_bnd,None,  'r')

    minQ=min(Q)*0.9
    maxQ=max(Q)*1.1
    minH=min(H)*0.9
    maxH=max(H)*1.1

    q_plot = np.linspace(minQ,maxQ,20)

    cshape = coef.shape
    row=cshape[0]
    for i in range(row):
        if coef[i,6] == 1:
            plt.plot(q_plot, coef[i,1]*q_plot + coef[i,4] + coef[i,0]*np.square(q_plot), 'r')
        else:
            plt.plot(q_plot, coef[i,1]*q_plot + coef[i,4] + coef[i,0]*np.square(q_plot), 'b')
    plt.plot(point_coordinates[0], point_coordinates[1], '*', ms=10, markerfacecolor='r', markeredgecolor='none')

    plt.xlim(minQ, maxQ)
    plt.ylim(minH, maxH)
    print("hello")
    plt.savefig('WorkingAreaBoundary3.png', bbox_inches='tight', pad_inches=0.1)


def _plotting_working_area_boundary(Q, H, E, working_area_coef, point_coordinates, Q3, H3):
    '''
    Plots the efficiency points within the working area, and plots the boundaries of the area.
    It plots also the user give boundaries. It can handle boundaries given in H.
    '''
    fig = plt.figure()
    CS = plt.scatter(Q, H, c=E, s=20, lw=0)
    plt.colorbar(CS, shrink=0.8, extend='both')
    plt.plot(point_coordinates[0], point_coordinates[1], '*', ms=10, markerfacecolor='r', markeredgecolor='none')
    evaluation_points = 10
    h_plot_values = np.linspace(np.amin(H3), np.amax(H3), evaluation_points)
    q_plot_values = np.linspace(np.amin(Q3), np.amax(Q3), evaluation_points)
    for k in range(working_area_coef.shape[0]):
        if abs(working_area_coef[k, 5]) < 1e-7:
            # Then it is given in Q
            plt.plot(q_plot_values, working_area_coef[k, 0]*np.square(q_plot_values)+working_area_coef[k, 1]
                     * q_plot_values+np.ones(evaluation_points)*working_area_coef[k, 4], 'k')
        else:
            plt.plot(working_area_coef[k, 2]*np.square(h_plot_values)+working_area_coef[k, 3] *
                     h_plot_values+np.ones(evaluation_points)*working_area_coef[k, 4], h_plot_values, 'k')
    plt.xlim(np.amin(Q3), np.amax(Q3)*1.1)
    plt.ylim(np.amin(H3), np.amax(H3)*1.1)
    plt.ylabel('Head [m]')
    plt.xlabel('Flow [$m^3\,s^{-1}$]')
    plt.title('Efficiency')
    plt.savefig('WorkingAreaBoundary.png', bbox_inches='tight', pad_inches=0.1)

def _write_working_area(working_area_coef):
    locations=[2, 1, 6, 3, 0] #2. number 1. q, 6. h2,   It says which locations should the given entries go, q2, q, h2, h, number
    solution_float=np.zeros(9)

    f = open('working_area_fit.txt', 'w+')
    f.write('working_area = ')
    for num, row in enumerate(working_area_coef):
        for k in range(len(locations)):
            if abs(float(working_area_coef[num, k]))> 1e-8:
                solution_float[locations[k]]=np.around(float(working_area_coef[num, k]),4)
            else:
                 solution_float[locations[k]] = 0.0

        to_print_sol=dot_aligned(solution_float)
        if num==0:
            f.write('{{{'+ to_print_sol[0]+',   '+to_print_sol[1]+',   '+to_print_sol[2]+'}'+',')
        else:
            f.write('                '+'{{'+ to_print_sol[0]+',   '+to_print_sol[1]+',   '+to_print_sol[2]+'}'+',')
        f.write('\n')
        f.write('                '+' {'+ to_print_sol[3]+',   '+to_print_sol[4]+',   '+to_print_sol[5]+'}'+',')
        f.write('\n')
        if num == len(working_area_coef[:,0])-1:
            f.write('                '+' {'+ to_print_sol[6]+',   '+to_print_sol[7]+',   '+to_print_sol[8]+'}}},')
        else:
            f.write('                '+' {'+ to_print_sol[6]+',   '+to_print_sol[7]+',   '+to_print_sol[8]+'}},')
        f.write('\n')

    f.write('\n')
    f.write('working_area_direction = {')
    for row in range(working_area_coef.shape[0]):
        if row < working_area_coef.shape[0]-1:
            if abs(working_area_coef[row,6])<1e-7:
                f.write(str(-1)  + ', ')
            else:
                f.write(str(1)  + ', ')
        else:
            if abs(working_area_coef[row,6])<1e-7:
                f.write(str(-1) )
            else:
                f.write(str(1)  )
    f.write('},')

def _write_power(working_area_coef):
    #locations=[2, 1, 6, 3, 0] #where does each element go q2, q, h2, h, number
    locations=[0, 1, 3]
    solution_float=np.zeros(9)

    f = open('power.txt', 'w+')
    f.write('power_coefficients = ')
    for num, row in enumerate(working_area_coef):
        for k in range(len(locations)):
            if abs(float(working_area_coef[num, k]))> 1e-8:
                solution_float[locations[k]]=np.around(float(working_area_coef[num, k]/1000),3)
            else:
                 solution_float[locations[k]] = 0.0

        to_print_sol=dot_aligned(solution_float)
        if num==0:
            f.write('{{{'+ to_print_sol[0]+',   '+to_print_sol[1]+',   '+to_print_sol[2]+'}'+',')
        else:
            f.write('                '+'{{'+ to_print_sol[0]+',   '+to_print_sol[1]+',   '+to_print_sol[2]+'}'+',')
        f.write('\n')
        f.write('                '+' {'+ to_print_sol[3]+',   '+to_print_sol[4]+',   '+to_print_sol[5]+'}'+',')
        f.write('\n')
        if num == len(working_area_coef[:,0])-1:
            f.write('                '+' {'+ to_print_sol[6]+',   '+to_print_sol[7]+',   '+to_print_sol[8]+'}}},')
        else:
            f.write('                '+' {'+ to_print_sol[6]+',   '+to_print_sol[7]+',   '+to_print_sol[8]+'}},')
        f.write('\n')

    f.write('\n')
    '''
    f.write('working_area_direction = {')
    for row in range(working_area_coef.shape[0]):
        if row < working_area_coef.shape[0]-1:
            if abs(working_area_coef[row,6])<1e-7:
                f.write(str(-1)  + ', ')
            else:
                f.write(str(1)  + ', ')
        else:
            if abs(working_area_coef[row,6])<1e-7:
                f.write(str(-1) )
            else:
                f.write(str(1)  )
    f.write('},')
    '''
def _write_approximation(solution, variable_approximated):

    locations=[2, 1, 6, 3, 4, 0]
    if 'speed' in variable_approximated:
        locations = [0, 1, 2, 3, 4, 5, 6, 7, 8]
    solution_float=np.zeros(9)
    for i, k in enumerate(solution):
        if abs(float(k))> 1e-8:
            if 'power' in variable_approximated:
               solution_float[locations[i]]=np.around(float(k)/1000,3)
            else:
               solution_float[locations[i]]=np.around(float(k),4)
        else:
            solution_float[locations[i]] = 0.0

    to_print_sol=dot_aligned(solution_float)
    f = open(variable_approximated +'.txt', 'w+')
    if 'power' in variable_approximated:
        f.write('power_coefficients = ')
    if 'speed' in variable_approximated:
        f.write('speed_coefficients = ')
    if 'efficiency' in variable_approximated:
        f.write('efficiency_coefficients = ')
    if 'speed' in variable_approximated:
        f.write('{{'+ to_print_sol[0]+',   '+to_print_sol[1]+',   '+to_print_sol[2]+'}'+',')
        f.write('\n')
        f.write('                     '+' {'+ to_print_sol[3]+',   '+to_print_sol[4]+',   '+to_print_sol[5]+'}'+',')
        f.write('\n')
        f.write('                     '+' {'+ to_print_sol[6]+',   '+to_print_sol[7]+',   '+to_print_sol[8]+'}},')
    else:
        f.write('{{{'+ to_print_sol[0]+',   '+to_print_sol[1]+',   '+to_print_sol[2]+'}'+',')
        f.write('\n')
        f.write('                     '+' {'+ to_print_sol[3]+',   '+to_print_sol[4]+',   '+to_print_sol[5]+'}'+',')
        f.write('\n')
        f.write('                     '+' {'+ to_print_sol[6]+',   '+to_print_sol[7]+',   '+to_print_sol[8]+'}}},')
    f.close()

def dot_aligned(seq):
    snums = ["{:.12g}".format(n) for n in seq]
    dots = [s.find('.') for s in snums]
    m = max(dots)
    m=8
    return [' '*(m - d) + s for s, d in zip(snums, dots)]


def _csv_reader(filename):
    # Reading the data from the excel file and storing it
    H = []
    Q = []
    P = []
    E = []

    extra_lines=0
    point_coordinates=[0, 0]
    pump_type=0
    loss_coefficient = 0
    maximum_rpm = 1.0
    maximum_calc_rpm=1.0
    constant_head = 0

    with open(filename + '.csv') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';')
        Counter = 0
        for row in spamreader:
            if Counter==0:
                for k in range(0,10):
                    if row[k]=='Data':
                        data_column=k
            if 'Dref' in row[data_column]:
                dref=float(row[data_column+1])
            if 'Nref' in row[data_column]:
                nref=float(row[data_column+1])
            if 'Loss h' in row[data_column]:
                losshref=float(row[data_column+1])
            if 'Loss q' in row[data_column]:
                lossqref=float(row[data_column+1])
            if 'Min eff' in row[data_column]:
                minimum_efficiency=float(row[data_column+1])
            if 'Min rpm' in row[data_column]:
                minimum_rpm=float(row[data_column+1])
            if 'Max rpm' in row[data_column]:
                maximum_calc_rpm=float(row[data_column+1])
            if 'Actual rpm' in row[data_column]:
                maximum_rpm=float(row[data_column+1])
            if 'Point' in row[data_column]:
                point_coordinates[0]=float(row[data_column+1])
                point_coordinates[1]=float(row[data_column+2])
            if 'Ackeret' in row[data_column]:
                ackeret_correction=int(row[data_column+1])
            if 'Extra' in row[data_column]:
                extra_lines=int(row[data_column+1])
                extra_line_coef=np.zeros((extra_lines,7))
            if 'Const head' in row[data_column]:
                constant_head = int(row[data_column+1])
                print ('read')
            if 'type' in row[data_column]:
                pump_type=int(row[data_column+1])
            if 'Loss coef' in row[data_column]:
                loss_coefficient = float(row[data_column + 1])
            for k in range(extra_lines):
                test_string='Line ' + str(k+1)
                if test_string in row[data_column]:
                    extra_line_coef[k,0]=float(row[data_column+1])
                    extra_line_coef[k,1]=float(row[data_column+2])
                    extra_line_coef[k,2]=float(row[data_column+3])
                    extra_line_coef[k,3]=float(row[data_column+4])
                    extra_line_coef[k,4]=float(row[data_column+5])


    with open(filename + '.csv') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';')
        Counter = 0
        for idx, row in enumerate(spamreader):
            if Counter==0:
                if row[0]=='Q':
                    Counter=1
            elif Counter > 0 and Counter < 100:
                if not row[0]:
                    Counter = 100
                elif float(row[0]) == 0:  #if the q is zero, we do not read that row
                    logger.warning('A row where Q=0 is skipped')
                elif row[2] and float(row[2])<=0:   #if the efficiency is negative, we do not read that row
                    logger.warning('A row with negative efficiency is skipped')
                elif row[3] and float(row[3])<=0:   #if the power is negative, we do not read that row
                     logger.warning('A row with negative power is skipped')
                else:
                    H.append(float(row[1]))
                    Q.append(float(row[0]))
                    if len(row[3]) == 0:
                        P.append(1000*9.81 * float(row[1]) * float(row[0]) / float(row[2]))
                        if len(row[2]) == 0:
                            E.append(1000*9.81 * float(row[1]) * float(row[0]) / float(row[3]))
                        else:
                            E.append(float(row[2]))

                    else:
                        P.append(float(row[3]))
                        if len(row[2]) == 0:
                            E.append(1000*9.81 * float(row[1]) * float(row[0]) / float(row[3]))
                        else:
                            E_read = float(row[2])
                            E_calculated = 1000*9.81 * float(row[1]) * float(row[0]) / float(row[3])
                            if abs(E_read - E_calculated) > 1e-3:
                                logger.error('Efficiency an power are not consistent in row: "{}".'.format(idx))
                                E.append(E_calculated)
                            else:
                                E.append(float(row[2]))

    N=nref
    number_of_points = len(H)
    P_=np.reshape(P,(number_of_points, 1))
    E_=np.reshape(E,(number_of_points, 1))
    Q_=np.reshape(Q,(number_of_points, 1))
    H_=np.reshape(H,(number_of_points, 1))
    if pump_type == 1:
        minimum_rpm = 1

    return P_, E_, Q_, H_, dref, nref, losshref, lossqref, minimum_efficiency, minimum_rpm, point_coordinates, ackeret_correction, extra_line_coef, pump_type, loss_coefficient, maximum_rpm, maximum_calc_rpm, constant_head




