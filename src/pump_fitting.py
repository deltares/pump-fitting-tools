'''
This code is reading a csv file with pump data and makes a convex fit.
csv_reader: if one of them (E or P ) is missing, it is calculated
Removing losses: if loss coef is given, E is recalculated
Generating points: in case of Ackeret correction the power is calculated again
If it is inconsistent in a row, calculated efficiency is used.
In this file you have the following functions:
- split_groups
- _approximating_power_linear
-  _approximating_power
- approximating_speed
- _approximating_wa_linear
- _approximating_polynoms_linear
- approximating_poplynoms
- approximaitng_polynoms_top
- removing_losses
- _restrict_points_for_woring_area
_boundary_point_detection2
_return_angle
- _restrict_points_efficiency
- generating points
_max_fit_error
_organize_wa_coef
_working_area_calc
_extra_area_calc
_2darray_to_np_flattened

'''

from sys import path
#path.append("D:\Casadi")
#! =====================
from casadi import *
import casadi as ca
from numpy import *
import scipy
import csv
import sys
import logging
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
logger = logging.getLogger("pump-fitting-tools")
logging.basicConfig(level=logging.INFO)

def split_groups(qv_flat, hv_flat, pv_flat, n, plot=False):
    # This function uses a clustering algorithm
    # It gives back a 1D array of labels (n, ) and a (cent_num, 3) of centroids
    # containing the q, h, p coordinates of the centroids
    from scipy.cluster.vq import kmeans, whiten, vq
    features = np.vstack((qv_flat, hv_flat, pv_flat)).transpose()
    # Whiten data
    whitened = whiten(features)
    # Find n clusters in the data
    # Pass array of K by M to be deterministic
    codebook, distortion = kmeans(whitened, whitened[:n, :])
    centroids = codebook / (whitened[0, :] / features[0, :])
    labels = vq(whitened, codebook)[0]
    # Plot data and cluster centers in red
    if plot:
        plt.scatter(features[:, 0], features[:, 1], s=2, c=labels)
        plt.scatter(centroids[:, 0], centroids[:, 1], s=4, c='r')
    return labels, centroids


def _approximating_power_linear(hv_data, qv_data, pv_data, use_constraints, type = 'power', constant_head = 0, max_rel_error=0.25, below_all_other=True, use_qpsol=False):

    qv_flat = qv_data.flatten()
    hv_flat = hv_data.flatten()
    pv_flat = pv_data.flatten()
    n = 5

    labels, centroids = split_groups(qv_flat, hv_flat, pv_flat, n, plot=True)
    n = centroids.shape[0]
    m = np.prod(pv_data.shape)
    assert centroids.shape[1] == 3, "Need Q, H and P column"

    # Each linear expression is "p = c[0] + c[1] * q + c[2] * h"
    coef = ca.SX.sym('coef', n, 3)

    # Creating symbolic variables for the power and the head
    q = ca.SX.sym('Q')
    h = ca.SX.sym('H')
    c = ca.SX.sym('c', 3)

    p_func = lambda q, h, c: c[0] + c[1] * q + c[2] * h

    # Creating the objective function
    f = 0.0

    # Minimize for the sum of the relative errors. Impose constraints on the maximum relative error.
    # We make an additional bunch of symbols to get the _absolute_ value of the relative error.
    # Note that we vectorize constraints as much as possible, to reduce the SWIG overhead when calling CasADi.
    constraints = []

    rel_err = ca.SX.sym("rel_err", m)
    abs_rel_err = ca.SX.sym("abs_rel_err", m)

    # Impose constraint on the maximum relative error
    if np.isfinite(max_rel_error):
        constraints.append((abs_rel_err, np.full(m, -np.inf), np.full(m, max_rel_error)))

    # Relate absolute relative error to the relative error
    constraints.append((abs_rel_err - rel_err, np.full(m, 0.0), np.full(m, np.inf)))
    constraints.append((abs_rel_err + rel_err, np.full(m, 0.0), np.full(m, np.inf)))

    # Assign relative error per group
    offset = 0
    for i in range(n):

        inds = (labels == i)
        m_i = sum(inds.astype(int))

        rel_error = (p_func(qv_data[inds], hv_data[inds], coef[i, :]) - pv_data[inds])/pv_data[inds]
        constraints.append((rel_err[offset:offset+m_i] - rel_error, np.full(m_i, 0.0), np.full(m_i, 0.0)))

        offset += m_i

    f = ca.sum1(abs_rel_err)

    # Force convexity
    for i in range(n):
        for j in range(n):
            if i == j:
                continue

            if below_all_other:
                # Force plane i to be below all points in group j
                inds = (labels == j)
                m_j = sum(inds.astype(int))

                constraints.append(((p_func(qv_data[inds], hv_data[inds], coef[i, :]) - pv_data[inds])/pv_data[inds], np.full(m_j, -np.inf), np.full(m_j, 0.0)))
            else:
                # Force plane i to be below the centroids of group j
                qj, hj, pj = centroids[j, :]
                constraints.append(((p_func(qj, hj, coef[i, :]) - pj)/pj, -np.inf, 0.0))

    g, lbg, ubg = zip(*constraints)
    g = ca.veccat(*g)
    lbg = ca.veccat(*lbg)
    ubg = ca.veccat(*ubg)

    X = ca.veccat(coef, rel_err, abs_rel_err)
    lbx = [-np.inf] * X.shape[0]
    ubx = [np.inf] * X.shape[0]

    nlp = {'f': f, 'g': g, 'x': X}

    if use_qpsol:
        solver = ca.qpsol('nlp', 'clp', nlp, {})
    else:
        options = {'print_time': False, 'ipopt': {'print_level': 0}}
        solver = ca.nlpsol('nlp', 'ipopt', nlp, options)

    results = solver(x0=np.zeros(X.shape), lbx=lbx, ubx=ubx, lbg=lbg, ubg=ubg)

    coef_eval = np.array(results['x']).flatten()[:np.prod(coef.shape)]
    coef_eval = coef_eval.reshape(n, 3, order='F')

    all_powers = []
    power_eval = coef_eval[0, 0] + coef_eval[0, 1] * qv_data + coef_eval[0, 2] * hv_data
    for i in range(1, n):
        power_eval = np.maximum(power_eval, coef_eval[i, 0] + coef_eval[i, 1] * qv_data + coef_eval[i, 2] * hv_data)

            # Calculating the error

    return coef_eval, power_eval, power_eval-pv_data

def _approximating_power(H, Q, P, use_constraints, type = 'power', constant_head = 0):
    coef = SX.sym('coef', 6)

    # Creating symbolic variables for the power and the head
    QQ = SX.sym('QQ')
    HH = SX.sym('HH')
    #print(constant_head)
    # Create symbolic funciton for the power
    if constant_head ==0:
        powerfun = coef[0]*QQ**2+coef[1]*QQ+coef[2]*HH**2+coef[3]*HH+coef[4]*QQ*HH+coef[5]
        print ('nonc h')
    else:
        powerfun = coef[0]*QQ**2+coef[1]*QQ+coef[5]
        print ( 'con h')

    hessian_power = ca.hessian(powerfun, ca.vertcat(QQ, HH))[0]

    power_function_symbolic = ca.Function('power_function_symbolic', [QQ, HH, coef], [powerfun])

    # Creating the objective function
    objective_function = SX.sym('objective_function')
    constraint_array =[]

    objective_function = 0

    for k in range(0, len(H)):
        objective_function = objective_function + ((power_function_symbolic(Q[k], H[k], coef)[0]-P[k])**2)
        constraint_array =ca.vertcat(constraint_array, power_function_symbolic(Q[k], H[k], coef)[0])
    if use_constraints == 1:
        constraint_array =ca.vertcat(ca.det(hessian_power), ca.trace(hessian_power) )
        objective_function = 0
        for k in range(0, len(H)):
            objective_function = objective_function + ((power_function_symbolic(Q[k], H[k], coef)[0]-P[k])**2)
            constraint_array =ca.vertcat(constraint_array, power_function_symbolic(Q[k], H[k], coef)[0])

            #nlp = {'x': coef, 'f': objective_function,  'g': constraint_array}
            nlp = {'f': objective_function, 'g':  constraint_array, 'x': coef}

            my_lower_bound = [0] * (2+len(Q))
            my_upper_bound = [inf]*(2+len(Q))
    else:
        #nlp = {'x': coef, 'f': objective_function,  'g': constraint_array}
        nlp = {'f': objective_function, 'g':  constraint_array, 'x': coef}

        my_lower_bound = [0] * (len(Q))
        my_upper_bound = [inf]*(len(Q))
    #options = {'print_time': 0, 'print_level': 0}
    options = {'print_time': False, 'ipopt': {'print_level': 0}}
    solver = ca.nlpsol('nlp', 'ipopt', nlp, options)
    #solver = NlpSolver('solver', 'ipopt', nlp, options)
    lbx = [-inf]
    ubx = [inf]
    lbg = my_lower_bound
    ubg = my_upper_bound
    results = solver(x0=np.zeros(coef.shape), lbx=lbx, ubx=ubx, lbg=lbg, ubg=ubg)

    objective_value = np.array(results['f']).flatten()

    solution = np.array(results['x']).flatten()

    print('Solution is:')
    print(solution)
    if 'power' in type:
        print("Convexity check, the numbers should be positive:")
        print (2*(solution[0]+solution[2]))
        print (4*(solution[0]*solution[2])-solution[4]**2)

    # Calculating the error

    power_calculated = np.array(H)
    error_calculated = np.array(H)
    for k in range(0, len(H)):
        error_calculated[k] = power_function_symbolic(Q[k], H[k], solution)[0]-P[k]
        power_calculated[k] = power_function_symbolic(Q[k], H[k], solution)[0]

    print ('The maximum error is: %5.3f, %5.3f percent.' % (max(error_calculated),
                                                              max(error_calculated)/power_calculated[np.argmax(error_calculated)]*100))
    print ('The minimum error is: %5.3f, %5.3f percent.' % (min(error_calculated),
                                                              min(error_calculated)/power_calculated[np.argmin(error_calculated)]*100))
    print ('The maximum value is: %5.3f.' % max(power_calculated))

    print ('The miniimum value is: %5.3f.' % min(power_calculated))

    return solution, power_calculated, error_calculated


def _approximating_speed(H, Q, P, use_constraints=0, type = 'speed'):
    coef = SX.sym('coef', 9)

    # Creating symbolic variables for the power and the head
    QQ = SX.sym('QQ')
    HH = SX.sym('HH')
    # Create symbolic funciton for the power
    powerfun = 0.0
    for i in range(3):
        for j in range(3):
            powerfun+=coef[i*3+j]*QQ**j*HH**i
    #powerfun = coef[0]*QQ**2+coef[1]*QQ+coef[2]*HH**2+coef[3]*HH+coef[4]*QQ*HH+coef[5]
    #p_hess = ca.hessian(p, ca.vertcat(q, h))[0]

    hessian_power = ca.hessian(powerfun, vertcat(QQ, HH))[0]

    power_function_symbolic = ca.Function('power_function_symbolic', [QQ, HH, coef], [powerfun])
    #qh_curve_symbolic = ca.Function('qh_curve_symbolic', [QQ, coef], [qh_curve])

    # Creating the objective function
    objective_function = SX.sym('objective_function')
    objective_function = 0

    for k in range(0, len(H)):
        objective_function = objective_function + ((power_function_symbolic(Q[k], H[k], coef)[0]-P[k])**2)

    my_lower_bound = [0] * (2)
    my_upper_bound = [inf]*(2)
    #print(det(hessian_power))
    #print(trace(hessian_power))
    constraints =ca.vertcat(ca.det(hessian_power), ca.trace(hessian_power))
    #constraints = vertcat([det(hessian_power), trace(hessian_power)])

    nlp = {'f': objective_function, 'g': [], 'x': coef}
    options = {'print_time': False, 'ipopt': {'print_level': 0}}
    solver = ca.nlpsol('nlp', 'ipopt', nlp, options)
    results = solver(x0=np.zeros(coef.shape))
    solution = np.array(results['x']).flatten()


    if use_constraints == 1:
        #nlp = {'x': coef, 'f': objective_function,  'g': constraints}
        nlp = {'f': objective_function, 'g': constraints, 'x': coef}
    else:
        #nlp = {'x': coef, 'f': objective_function}
        nlp = {'f': objective_function, 'g': [], 'x': coef}
        my_lower_bound = []
        my_upper_bound = []
    #options = {'print_time': 0, 'print_level': 0}
    options = {'print_time': False, 'ipopt': {'print_level': 0}}

    #solver = NlpSolver('solver', 'ipopt', nlp, options)
    solver = ca.nlpsol('nlp', 'ipopt', nlp, options)


    lbx = [-inf]
    ubx = [inf]

    lbg = my_lower_bound
    ubg = my_upper_bound
    results = solver( lbx=lbx, ubx=ubx, lbg=lbg)

    results = solver(x0=np.zeros(coef.shape), lbx=lbx, ubx=ubx, lbg=lbg, ubg=ubg)

    #solver.setInput(lbx, 'lbx')
    #solver.setInput(ubx, 'ubx')
    #solver.setInput(lbg, 'lbg')
    #solver.setInput(ubg, 'ubg')
    #solver.evaluate()
    #objective_value = float(solver.getOutput('f')[0])
    objective_value = np.array(results['f']).flatten()

    #solution = solver.getOutput('x')
    solution = np.array(results['x']).flatten()

    print('Solution is:')
    print(solution)
    #if 'power' in type:
    #    print("Convexity check, the numbers should be positive:")
    #    print 2*(solution[0]+solution[2])
    #    print 4*(solution[0]*solution[2])-solution[4]**2
    #    #print solution

    # Calculating the error

    power_calculated = np.array(H)
    error_calculated = np.array(H)
    for k in range(0, len(H)):
        error_calculated[k] = power_function_symbolic(Q[k], H[k], solution)[0]-P[k]
        #error_calculated[k] = power_function_symbolic([Q[k], H[k], solution])[0]-P[k]
        #power_calculated[k] = power_function_symbolic([Q[k], H[k], solution])[0]
        power_calculated[k] = power_function_symbolic(Q[k], H[k], solution)[0]

    print ('The maximum error is: %5.3f, %5.3f percent.' % (max(error_calculated),
                                                              max(error_calculated)/power_calculated[np.argmax(error_calculated)]*100))
    print ('The minimum error is: %5.3f, %5.3f percent.' % (min(error_calculated),
                                                              min(error_calculated)/power_calculated[np.argmin(error_calculated)]*100))
    print ('The maximum value is: %5.3f.' % max(power_calculated))

    return solution, power_calculated, error_calculated

def _approximating_wa_linear(H, Q, n=5):
    #This function approximates the working area with linear bounds
    #Q and H should be the boundary points of the working area

    #Create unite steps to normalize the Q-H distance
    Q_unit=( max(Q)-min(Q))/10
    H_unit=( max(H)-min(H))/10
    #Initialize some variables
    #Q_bnd = np.zeros_like(Q)
    #H_bnd = np.zeros_like(H)
    point_array=np.zeros((size(Q),9))

    # The first step is to order the points
    #First, an array is made where the first column is the index of the points
    #and the other columns contain the points that are close to it
    for k in range(size(Q)):
        min_distance=1000
        min_distance_index=0
        distance_mat = np.zeros_like(Q)
        for kk in range(0, size(Q)):
            if kk==k:
                distance_mat[kk] = 1000
            else:
                distance_mat[kk] = (Q[kk]-Q[k])**2/Q_unit**2+(H[kk]-H[k])**2/H_unit**2

            indices_all = np.argsort(distance_mat)
        point_array[k,0] = k
        for j in range(1,9):
            point_array[k,j] = indices_all[j-1]

    #This part orders the points
    #It reads from the array the closest point, and puts is as next
    #If the closes point is already used, then it puts the second closest point etc.
    ordered_index = np.zeros_like(Q)
    ordered_index [0] = point_array[0,0]
    ordered_index[1] = point_array[0,1]

    for k in range(1,size(Q)):
        #find it in distance_mat find the location of ordered_index[1] in the first row
        i = np.where(point_array[:,0] == ordered_index[k])
        candidate_number=point_array[i,1]

        if sum(np.isin(ordered_index, candidate_number))>0:
            candidate_number=point_array[i,2]
            if sum(np.isin(ordered_index, candidate_number))>0:
                candidate_number=point_array[i,3]
                if sum(np.isin(ordered_index, candidate_number))>0:
                    candidate_number=point_array[i,4]
                    if sum(np.isin(ordered_index, candidate_number))>0:
                        candidate_number=point_array[i,5]
                        if sum(np.isin(ordered_index, candidate_number))>0:
                            candidate_number=point_array[i,6]
                            if sum(np.isin(ordered_index, candidate_number))>0:
                                candidate_number=point_array[i,7]
                                if sum(np.isin(ordered_index, candidate_number))>0:
                                    candidate_number=point_array[i,8]
        #Find index of candidate number
        if k < size(Q)-1:
            ordered_index[k+1]=candidate_number

    unique, u_ind, u_inv, unique_counts = np.unique(ordered_index, return_index=True, return_inverse=True, return_counts=True)
    #check if the first column is availabe

    ordered_index_int = np.zeros((size(Q),), dtype=int)
    for k in range(size(ordered_index)):
        ordered_index_int[k] = int(ordered_index[k])

    #These variables now are the ordered points
    Q_bnd = np.take(Q,ordered_index_int)
    H_bnd = H[ordered_index_int]

    #This part puts the points to clusters related to the difference between
    #the direction of the vectors between them
    cluster_number = 0
    angle_previous = 0
    new_labels = np.zeros_like(Q_bnd)

    angle_tolerance = 10 #This is the minimum angular difference (degrees) to start a new cluster
    for k in range(size(Q_bnd)-1):
        angle1 = _return_angle(Q_bnd[k+1]-Q_bnd[k],H_bnd[k+1]-H_bnd[k])
        if k> 0:
            if abs(angle_previous - angle1) < angle_tolerance:
                new_labels[k+1]=cluster_number
            else:
                cluster_number = cluster_number + 1
                new_labels[k+1]=cluster_number
        angle_previous = angle1
    labels = new_labels

    qv_flat = Q_bnd
    hv_flat = H_bnd

    qv_data= Q_bnd
    hv_data = H_bnd

    features = np.vstack((qv_flat, hv_flat)).transpose()
    unique, u_ind, u_inv, unique_counts = np.unique(labels, return_index=True, return_inverse=True, return_counts=True)

    #This part counts the labels belonging to only one point and removes them
    unique_labels=[]
    for k, member in enumerate(unique_counts):
        if member == 1:
            unique_labels.append(unique[k])
    # Plot data and cluster centers in red
    plt.scatter(features[:, 0], features[:, 1], s=10, c=labels, cmap=cm.tab10)

    # Each linear expression is "p = c[0] + c[1] * q"
    n = size(unique)
    coef = ca.SX.sym('coef', n, 2)

    # Creating symbolic variables for the power and the head
    q = ca.SX.sym('Q')
    c = ca.SX.sym('c', 2)

    h = c[0] + c[1] * q

    h_func = ca.Function('head_function', [q, c], [h])

    # Creating the objective function
    f = 0.0

    for i in range(n):
        uswitch=0
        for k in range(size((unique_labels))):
            if unique_labels[k] ==i:
                uswitch=1
        if uswitch ==0:
            inds = (labels == i)
            for hi, qi in zip(hv_data[inds], qv_data[inds]):
                f += ((h_func(qi, coef[i, :])-hi))**2

    X = ca.veccat(coef)
    lbx = [-np.inf] * X.shape[0]
    ubx = [np.inf] * X.shape[0]

    nlp = {'f': f, 'x': X}

    options = {'print_time': False, 'ipopt': {'print_level': 0}}
    solver = ca.nlpsol('nlp', 'ipopt', nlp, options)
    results = solver(x0=np.zeros(X.shape), lbx=lbx, ubx=ubx)
    coef_eval = np.array(results['x']).flatten()
    coef_eval = coef_eval.reshape(n, 2, order='F')
    all_powers = []
    head_eval = coef_eval[0, 0] + coef_eval[0, 1] * qv_data
    head_eval = np.zeros((len(qv_data),n))
    for i in range(0, n):
        head_eval[:,i] = coef_eval[i, 0] + coef_eval[i, 1] * qv_data
    qp = np.linspace(0.0, 1, 20)
    collength= np.shape(coef_eval)[0]
    zerocol = np.zeros((collength, 1))
    coef_eval_flipped = np.append(np.transpose([coef_eval[:,1]]), np.transpose([coef_eval[:,0]]), axis=1)
    new_coef = np.append(zerocol, coef_eval_flipped, axis=1)

    #This part removed the lines with zero
    co_sh= shape(new_coef)
    co_row=co_sh[0]
    co_col = co_sh[1]


    row_counter = 0
    for row in range(co_row):
        zero_row = 1
        for element in range(co_col):
            if abs(new_coef[row,element])>1e-7:
                zero_row = 0
        if zero_row ==0:
            row_counter = row_counter +1

    trimmed_coef = np.zeros((row_counter, co_col))
    row_counter = 0
    for row in range(co_row):
        zero_row = 1
        for element in range(co_col):
            if abs(new_coef[row,element])>1e-7:
                zero_row = 0
        if zero_row ==0:
            trimmed_coef[row_counter,:] = new_coef[row,:]
            row_counter = row_counter +1
    #This part changes the shape to be consistent with the 7 column array in excel
    coef_shape=shape(trimmed_coef)
    new_working_area_coef = np.zeros((coef_shape[0]  ,7))
    new_working_area_coef[:,4] = trimmed_coef[:,2]
    new_working_area_coef[:,1] = trimmed_coef[:,1]
    new_working_area_coef[:,3] = -1


    return  new_working_area_coef

def _approximating_polynoms_linear(H, Q, convexity_parameter, constant_speed=0):
    qv_flat = Q
    hv_flat = H

    qv_data= Q
    hv_data = H
    if convexity_parameter == -1:
        n = 1
    else:
        n = 3
    from scipy.cluster.vq import kmeans, whiten, vq
    features = np.vstack((qv_flat, hv_flat)).transpose()
    # Whiten data
    whitened = whiten(features)
    # Find n clusters in the data
    codebook, distortion = kmeans(whitened, n)
    centroids = codebook / (whitened[0, :] / features[0, :])
    labels = vq(whitened, codebook)[0]
    # Plot data and cluster centers in red
    plt.scatter(features[:, 0], features[:, 1], s=2, c=labels)
    plt.scatter(centroids[:, 0], centroids[:, 1], s=4, c='r')

    #plt.show()
    n = centroids.shape[0]
    assert centroids.shape[1] == 2, "Need Q and H column"

    # Each linear expression is "p = c[0] + c[1] * q"
    coef = ca.SX.sym('coef', n, 2)

    # Creating symbolic variables for the power and the head
    q = ca.SX.sym('Q')
    c = ca.SX.sym('c', 2)

    h = c[0] + c[1] * q

    h_func = ca.Function('head_function', [q, c], [h])

    # Creating the objective function
    f = 0.0

    for i in range(n):
        inds = (labels == i)

        for hi, qi in zip(hv_data[inds], qv_data[inds]):
            f += ((h_func(qi, coef[i, :])-hi))**2
    # Force convexity
    g = []
    for i in range(n):
        for j in range(n):
            if i == j:
                continue
            qj, hj = centroids[j, :]
            if convexity_parameter == 1:
                g.append(h_func(qj, coef[i, :]) - hj)
            else:
                g.append(-h_func(qj, coef[i, :]) + hj)

    # lbg = np.full(len(g), -np.inf)
    # ubg = np.zeros(len(g))
    ubg = np.full(len(g), np.inf)
    lbg = np.zeros(len(g))

    g = ca.veccat(*g)

    X = ca.veccat(coef)
    lbx = [-np.inf] * X.shape[0]
    ubx = [np.inf] * X.shape[0]

    nlp = {'f': f, 'g': g, 'x': X}

    options = {'print_time': False, 'ipopt': {'print_level': 0}}
    solver = ca.nlpsol('nlp', 'ipopt', nlp, options)
    results = solver(x0=np.zeros(X.shape), lbx=lbx, ubx=ubx, lbg=lbg, ubg=ubg)

    coef_eval = np.array(results['x']).flatten()
    coef_eval = coef_eval.reshape(n, 2, order='F')

    all_powers = []
    head_eval = coef_eval[0, 0] + coef_eval[0, 1] * qv_data
    head_eval = np.zeros((len(qv_data),n))
    for i in range(0, n):
        #head_eval = np.maximum(head_eval, coef_eval[i, 0] + coef_eval[i, 1] * qv_data)
        #head_eval = np.maximum(head_eval, coef_eval[i, 0] + coef_eval[i, 1] * qv_data)
        head_eval[:,i] = coef_eval[i, 0] + coef_eval[i, 1] * qv_data

        #head_eval1 = coef_eval[1, 0] + coef_eval[1, 1] * qv_data
        #head_eval2 = coef_eval[2, 0] + coef_eval[2, 1] * qv_data
        #head_eval3 = coef_eval[0, 0] + coef_eval[0, 1] * qv_data
    #diff_thershold=1e-3
    #if abs(coef_eval[0,0]-coef_eval[1,0])<diff_thershold and abs(coef_eval[0,1]-coef_eval[1,1])<diff_thershold:
    #    coef_eval = coef_eval[1:,:]
    #print(head_eval)
    qp = np.linspace(0.0, 1, 20)
    #print(shape(qp))
    #print(shape(head_eval))
    #print(coef_eval)
    plt.plot(qv_data, head_eval)
    #plt.plot(qv_data, head_eval1)
    #plt.plot(qv_data, head_eval2)
    #plt.plot(qv_data, head_eval3)

    #plt.show()

    collength= np.shape(coef_eval)[0]
    zerocol = np.zeros((collength, 1))
    coef_eval_flipped = np.append(np.transpose([coef_eval[:,1]]), np.transpose([coef_eval[:,0]]), axis=1)
    new_coef = np.append(zerocol, coef_eval_flipped, axis=1)
    return  new_coef
    # q squared, q, simple

def _approximating_polynoms(H, Q, convexity_parameter, constant_speed=0):
    #This function gives back the coefficients of a second order convex polynom
    #It is expressed in Q
    # conveiity is 1 if convex and 0 if concave
    #First it makes a fit, and then it checks if it is convex.
    #If not, it makes a linear fit.

    coef = ca.SX.sym('coef', 3)
    QQ = ca.SX.sym('QQ')

    # Create symbolic funciton for the power
    if constant_speed == 0 or constant_speed == 2:
        qh_curve = coef[0]*QQ**2+coef[1]*QQ+coef[2]
    elif constant_speed == 1:
        qh_curve = coef[1]*QQ+coef[2]
    else:
        logger.warning("Constant speed chosing number must be 1, 2 or 0")
        qh_curve = coef[0]*QQ**2+coef[1]*QQ+coef[2]

    qh_curve_symbolic = ca.Function('qh_curve_symbolic', [QQ, coef], [qh_curve])

    # Creating the objective function
    objective_function = SX.sym('objective_function')
    objective_function = 0

    for k in range(0, len(H)):
        #objective_function = objective_function + ((qh_curve_symbolic((Q[k], coef)[0]-H[k])**2)
        objective_function = objective_function + ( qh_curve_symbolic(Q[k], coef)[0] -H[k] ) **2

    #nlp = {'x': coef, 'f': objective_function}

    nlp = {'f': objective_function, 'g': [], 'x': coef}

    options = {'print_time': False, 'ipopt': {'print_level': 0}}
    solver = ca.nlpsol('nlp', 'ipopt', nlp, options)

    results = solver(ubg = 0)
    solution = np.array(results['x']).flatten()

    objective_value = np.array(results['f']).flatten()

    #Here is checked if the solution is convex. If not, we make a linear fit
    if solution[0]*convexity_parameter < 0: #or solution[1]*convexity_parameter < 0:
        print("First attempt was not convex")
        qh_curve = coef[1]*QQ+coef[2]
        #qh_curve_symbolic = SXFunction('qh_curve_symbolic', [QQ, coef], [qh_curve])
        qh_curve_symbolic = ca.Function('qh_curve_symbolic', [QQ, coef], [qh_curve])

        # Creating the objective function
        objective_function = SX.sym('objective_function')
        objective_function = 0

        for k in range(0, len(H)):
            #objective_function = objective_function + ((qh_curve_symbolic([Q[k], coef])[0])-H[k])**2
            objective_function = objective_function + ( qh_curve_symbolic(Q[k], coef)[0] -H[k] ) **2


        nlp = {'f': objective_function, 'x': coef}
        options = {'print_time': False, 'ipopt': {'print_level': 0}}
        solver = ca.nlpsol('nlp', 'ipopt', nlp, options)
        #results = solver(x0=np.zeros(coef.shape))
        results = solver()

        solution = np.array(results['x']).flatten()

    polynom_coef = [float(solution[0]), float(solution[1]), float(solution[2])]
    polynom_coef = numpy.reshape( numpy.asarray(polynom_coef   ), (1,shape(numpy.asarray(polynom_coef))[0]))
    return polynom_coef, convexity_parameter
    # It is the opposite, it starts with the square

def _approximate_convex_extra_lines(extra_line_coef, Q):
    print(extra_line_coef)
    cshape = extra_line_coef.shape
    row=cshape[0]
    convex_polynom_coef_array= np.zeros((row,3))
    convexity_parameter_array=np.zeros((row,1))
    q_points = np.linspace(min(Q),max(Q),20)
    for k in range(row):
        h_points = np.square(q_points)*extra_line_coef[k,1]+q_points * extra_line_coef[k,1] + extra_line_coef[k,4]
        convex_polynom_coef, convexity_parameter=  _approximating_polynoms(h_points, q_points, -1, 0)
        convexity_parameter_array [k] = convexity_parameter
        convex_polynom_coef_array[k,:] = convex_polynom_coef
    print(convex_polynom_coef_array)
    return convex_polynom_coef_array, convexity_parameter_array

def _approximating_polynoms_top(H, Q, convexity_parameter, constant_speed=0):
    #This function gives back the coefficients of a second order convex polynom
    #It is expressed in Q
    # conveiity is 1 if convex and 0 if concave
    #First it makes a fit, and then it checks if it is convex.
    #If not, it makes a linear fit.

    coef = ca.SX.sym('coef', 3)
    QQ = ca.SX.sym('QQ')

    # Create symbolic funciton for the power
    if constant_speed == 0 or constant_speed == 2:

        qh_curve = coef[0]*QQ**2+coef[1]*QQ+coef[2]
    elif constant_speed == 1:
        qh_curve = coef[1]*QQ+coef[2]
    else:
        logger.warning("Constant speed chosing number must be 1, 2 or 0")
        qh_curve = coef[0]*QQ**2+coef[1]*QQ+coef[2]
        #qh_curve_symbolic = SXFunction('qh_curve_symbolic', [QQ, coef], [qh_curve])

    qh_curve_symbolic = ca.Function('qh_curve_symbolic', [QQ, coef], [qh_curve])

    # Creating the objective function
    objective_function = SX.sym('objective_function')
    objective_function = 0

    #print(  qh_curve_symbolic([Q[0], coef])  )

    for k in range(0, len(H)):
        #objective_function = objective_function + ((qh_curve_symbolic((Q[k], coef)[0]-H[k])**2)
        objective_function = objective_function + ( qh_curve_symbolic(Q[k], coef)[0] -H[k] ) **2

    #nlp = {'x': coef, 'f': objective_function}

    nlp = {'f': objective_function, 'g': -coef[2], 'x': coef}
    options = {'print_time': False, 'ipopt': {'print_level': 0}}
    solver = ca.nlpsol('nlp', 'ipopt', nlp, options)
    results = solver(x0=np.zeros(coef.shape))
    solution = np.array(results['x']).flatten()

    #solution = solution.reshape(n, 2, order='F')

    #nlp = {'f': objective_function, 'g': g, 'x': coef}
    #options = {'print_time': False, 'ipopt': {'print_level': 0}}
    #solver = ca.nlpsol('nlp', 'ipopt', nlp, options)
    #results = solver(x0=np.zeros(coef.shape), lbx=lbx, ubx=ubx, lbg=lbg, ubg=ubg)




    #options = {'print_time': 0, 'print_level': 0}
    #solver = NlpSolver('solver', 'ipopt', nlp, options)
    #solver.setInput(lbg, 'lbg')
    #solver.setInput(0, 'ubg')
    #solver.evaluate()
    objective_value = np.array(results['f']).flatten()
    #solution = solver.getOutput('x')

    #Here is checked if the solution is convex. If not, we make a linear fit
    if solution[0]*convexity_parameter < 0: #or solution[1]*convexity_parameter < 0:
        qh_curve = coef[1]*QQ+coef[2]
        #qh_curve_symbolic = SXFunction('qh_curve_symbolic', [QQ, coef], [qh_curve])
        qh_curve_symbolic = ca.Function('qh_curve_symbolic', [QQ, coef], [qh_curve])

        # Creating the objective function
        objective_function = SX.sym('objective_function')
        objective_function = 0

        for k in range(0, len(H)):
            #objective_function = objective_function + ((qh_curve_symbolic([Q[k], coef])[0])-H[k])**2
            objective_function = objective_function + ( qh_curve_symbolic(Q[k], coef)[0] -H[k] ) **2


        nlp = {'f': objective_function, 'g': -coef[2], 'x': coef}
        options = {'print_time': False, 'ipopt': {'print_level': 0}}
        solver = ca.nlpsol('nlp', 'ipopt', nlp, options)
        results = solver(x0=np.zeros(coef.shape))
        solution = np.array(results['x']).flatten()

        #nlp = {'x': coef, 'f': objective_function, }
        #options = {'print_time': 0, 'print_level': 0}
        #solver = NlpSolver('solver', 'ipopt', nlp, options)
        #solver.evaluate()
        #objective_value = float(solver.getOutput('f')[0])
        #solution = solver.getOutput('x')

    polynom_coef = [float(solution[0]), float(solution[1]), float(solution[2])]
    polynom_coef = numpy.reshape( numpy.asarray(polynom_coef   ), (1,shape(numpy.asarray(polynom_coef))[0]))
    return polynom_coef


def _removing_losses(Hman, Q, losshref, lossqref, loss_coefficient, E, P):
    #This function removes the head losses, that is calculated the static head
    #by using the equation Hstat = Hman - H losses
    #H losses can be added in two ways: either with a loss coefficient or
    # a known loss for a known Q
    #In the end of the file the efficiency is calculated. It is needed because for the
    #power approximation we need static efficiency, and maybe it was manometric before
    Hstat = np.empty_like(Hman)
    if lossqref < 1e-7:
        Hstat = Hman
    else:
        for k, h in enumerate(Hman):
            Hstat[k] = Hman [k] - losshref*(Q[k]/lossqref)**2 - loss_coefficient * Q[k]**2
            if Hstat[k] < 0:
                logger.error('Too much losses, H became negative')
            E[k] = 1000 * 9.81 * Hstat[k] * Q[k] / P[k]
    return Hstat, E


def _restrict_points_for_woring_area(H3, Q3, P3, E3, N3, working_area_coef, maxfiterror, pump_type):
    #This part creates the restricted points for the third time
    #The data about the array is stored in the [5] and [6] entry
    #if [5] is 1 is H boundary, if it is 0, then Q
    #if [6] is 1 is top boundary, it is 0, bottom boundary
    append_array_index=np.ones_like(H3)
    diff_q_tolerance=0.25
    diff_h_tolerance=maxfiterror
    diff_h_tolerance=0.25

    if pump_type == 1:
        diff_q_tolerance=0.5
        diff_h_tolerance=0.5

    for i in range(0, shape(H3)[0]):
    #for i in range(0, 1):
        for ii in range(0, shape(H3)[1]):
        #for ii in range(0, 1):
            append_it=1
            h_coord=H3[i,ii]
            q_coord=Q3[i,ii]
            for row in range(working_area_coef.shape[0]):
                if abs(working_area_coef[row, 5])<1e-7:
                    #we have q, we evaluate the polynom plugging in q
                    poly_h_value=working_area_coef[row,0]*q_coord**2+working_area_coef[row, 1]*q_coord+working_area_coef[row,4]
                    #print poly_h_value
                    if abs(working_area_coef[row, 6])<1e-7:
                        #bottom boundary, point coordinate should be above it (disqualified if it is below)
                        #print diff_h_tolerance
                        if h_coord < poly_h_value-diff_h_tolerance:
                            append_it=0
                    else:
                        #lower boundary, the point coordinate should be under it, if not, gets a "no" for joining
                        if h_coord > poly_h_value + diff_h_tolerance:
                            append_it=0
                else:
                    #we have h, we evaluate the polynom plugging in h
                    poly_q_value=working_area_coef[row,2]*h_coord**2+working_area_coef[row, 3]*h_coord+working_area_coef[row,4]
                    if abs(working_area_coef[row, 6])<1e-7:
                        #lower boundary
                        if q_coord < poly_q_value - diff_q_tolerance:
                            append_it=0
                    else: #upper boudnary
                        if q_coord > poly_q_value + diff_q_tolerance:
                            append_it=0
                #print append_it
            append_array_index[i,ii]=append_it

    Q=[]
    H=[]
    E=[]
    P=[]
    N=[]

    for i in range(0, shape(H3)[0]):
        for ii in range(0, shape(H3)[1]):
            if append_array_index[i,ii]==1:
                H.append(H3[i, ii])
                Q.append(Q3[i, ii])
                P.append(P3[i, ii])
                E.append(E3[i, ii])
                N.append(N3[i, ii])

    number_of_points = len(H)
    P_=ravel(np.reshape(P,(number_of_points, 1)))
    E_=ravel(np.reshape(E,(number_of_points, 1)))
    Q_=ravel(np.reshape(Q,(number_of_points, 1)))
    H_=ravel(np.reshape(H,(number_of_points, 1)))
    N_=ravel(np.reshape(N,(number_of_points, 1)))

    return Q_, H_, E_, P_, N_


def  _boundary_point_detection2(Q, H):
    bounadary_label = np.zeros_like(H)
    q_bnd = []
    h_bnd = []
    delta_direction = 120
    direction_step = 60
    for idx, q_coord in enumerate(Q):
    #for idx in range(190,191):

        h_coord = H[idx]
        q_coord = Q[idx]

        boundary_switch = 0 #rue, boundary

        for direction in np.arange(0,360,direction_step):
            min_direction = direction - delta_direction/2
            max_direction = direction + delta_direction/2
            direction_switch = 0 #IF it becomes 1, than this is not any more a boundary direction

            for idx_check, q_check_coord in (enumerate(Q)):
                h_check_coord = H[idx_check]
                q_check_coord = Q[idx_check]
                #Calculate the direction of the vector pointing at the checks
                if q_check_coord == q_coord and h_check_coord == h_coord:
                    test_angle = direction + delta_direction
                else:
                    test_angle = _return_angle(q_check_coord-q_coord,h_check_coord-h_coord)
                if test_angle < max_direction and test_angle > min_direction:
                    #boundary_switch = 0  #There is a neigbouring point in that direction
                    direction_switch = 1
            if direction_switch == 0:
                boundary_switch = 1

        bounadary_label[idx] = boundary_switch

    for idx, number in enumerate(bounadary_label):
        if number == 1:
            h_bnd.append(H[idx])
            q_bnd.append(Q[idx])
    q_bnd = numpy.asarray(q_bnd)
    h_bnd = numpy.asarray(h_bnd)

    return bounadary_label, q_bnd, h_bnd

def _return_angle(Q,H):
    v1 = [0,1]
    v2 = [Q,H]
    v1_u = v1 / np.linalg.norm(v1)
    v2_u = v2 / np.linalg.norm(v2)
    if v2[0] > 0:
        degrees = np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))*180/np.pi
    else:
        degrees = 360- np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))*180/np.pi
    return degrees
    #return 1

def _restrict_points_efficiency(H3, Q3, E3, P3, N3, minimum_efficiency):
    # Creating the simpl ones again
    H = []
    Q = []
    P = []
    E = []
    N = []
    left_eff_line_h = []
    left_eff_line_q = []

    right_eff_line_h = []
    right_eff_line_q = []


    if E3[0,0] < minimum_efficiency:
        start_switch = 0
    else:
        start_switch = 1
        left_eff_line_h = H3[0, ]
        left_eff_line_q = Q3[0, ]
        start_index = 0


    if E3[-1,-1] < minimum_efficiency:
        end_switch = 0
    else:
        end_switch = 1
        right_eff_line_h = H3[-1, ]
        right_eff_line_q = Q3[-1, ]
        finish_index = shape(H3)[0]

    # This block selects the points that are relevant for the working area
    ii = 1
    for i in range(0, shape(H3)[0]):
        if start_switch == 0 and E3[i, ii] >= minimum_efficiency:
            start_switch = 1
            left_eff_line_h = (H3[i, ])
            left_eff_line_q = (Q3[i, ])
            start_index = i
        if (end_switch == 0 and start_switch == 1) and (E3[i, ii] < minimum_efficiency and i > 0):
            end_switch = 1
            finish_index = i-1
            right_eff_line_h = (H3[i-1, ])
            right_eff_line_q = (Q3[i-1, ])

    #--------------------------------------------------------------

    for i in range(0, shape(H3)[0]):
        for ii in range(0, shape(H3)[1]):
            if i >= start_index and i <= finish_index:
                H.append(H3[i, ii])
                Q.append(Q3[i, ii])
                P.append(P3[i, ii])
                E.append(E3[i, ii])
                N.append(N3[i, ii])


    # Using the indices found
    bottom_rpm_line_h = H3[start_index:finish_index+1, 0]
    bottom_rpm_line_q = Q3[start_index:finish_index+1, 0]

    top_rpm_line_h = H3[start_index:finish_index+1, -1]
    top_rpm_line_q = Q3[start_index:finish_index+1, -1]

    #return H, Q, P, E, N, bottom_rpm_line_h, bottom_rpm_line_q, top_rpm_line_h, top_rpm_line_q, left_eff_line_h, left_eff_line_q, right_eff_line_h, right_eff_line_q
    restricted_area = {'H':H, 'Q':Q, 'P':P, 'E':E,'N':N, 'bottom_rpm_line_h':bottom_rpm_line_h, 'bottom_rpm_line_q':bottom_rpm_line_q, 'top_rpm_line_h': top_rpm_line_h, 'top_rpm_line_q': top_rpm_line_q, 'left_eff_line_h':left_eff_line_h, 'left_eff_line_q': left_eff_line_q, 'right_eff_line_h':right_eff_line_h, 'right_eff_line_q': right_eff_line_q}
    #restricted_area = dict();
    #restricted_area['bottom_rpm_line_h'] = bottom_rpm_line_h
    return restricted_area

def _generating_points(H, Q, E, N, P, number_generated_points, pump_type, ackeret_correction, minimum_rpm, maximum_rpm, maximum_calc_rpm=1.0):
    #The given efficiency is only used if there is no Ackeret correction.
    NlistProp = np.linspace(minimum_rpm, maximum_calc_rpm, number_generated_points)
    H3 = np.zeros((len(H), number_generated_points))
    Q3 = np.zeros((len(H), number_generated_points))
    P3 = np.zeros((len(H), number_generated_points))
    E3 = np.zeros((len(H), number_generated_points))
    N3 = np.zeros((len(H), number_generated_points))

    for i in range(0, len(H)):
        for ii in range(0, number_generated_points):
            Nnew = NlistProp[ii]*N / maximum_rpm
            if pump_type == 2:
               Q3[i, ii] = Q[i]*Nnew/N
               H3[i, ii] = H[i]
               P3[i, ii] = P[i]*(Nnew/N)
            else:
               Q3[i, ii] = Q[i]*Nnew/N
               H3[i, ii] = H[i]*(Nnew/N)**2
               P3[i, ii] = P[i]*(Nnew/N)**3
            E3[i, ii] = E[i]
            N3[i, ii] = Nnew

            if ackeret_correction == 1:
                #Here we recalculate the efficiency so that it is the one that corresponds to the H
                #that already includes losses. This might only be necessary if losses added as extra.
                E3[i, ii] = 1000 * 9.81 * H3[i,ii] * Q3[i, ii]/(P3[i, ii])
                alpha = 0.15
                corrected_efficiency = 1.0 - (1.0-E3[i, ii])*pow((N/Nnew), alpha)
                if corrected_efficiency < -1.0:
                    E3[i, ii] = -1.0
                elif corrected_efficiency > 1.0:
                    E3[i, ii] = 1.0
                else:
                    E3[i, ii] = corrected_efficiency
                # Recalculating the power with the corrected efficiency, in this way the Ackeret correction
                # is inlcuded in the power calculation
                P3[i, ii] = 1000 * 9.81 * H3[i, ii]* Q3[i, ii]/(E3[i, ii] )

    return H3, Q3, E3, N3, P3

def _max_fit_error(bottom_rpm_line_q, bottom_rpm_line_h, top_rpm_line_q, top_rpm_line_h, left_eff_line_q, left_eff_line_h, right_eff_line_q, right_eff_line_h,
                       bottom_polynom_coef, top_polynom_coef, left_polynom_coef, right_polynom_coef):
    top_polynom_max_error = np.amax(np.absolute(
        (top_polynom_coef[0, 0]*np.square(top_rpm_line_q) + top_polynom_coef[0, 1]*top_rpm_line_q+top_polynom_coef[0, 2])-top_rpm_line_h))
    bottom_polynom_max_error = np.amax(np.absolute((bottom_polynom_coef[0,
                                       0]*np.square(bottom_rpm_line_q) + bottom_polynom_coef[0,1]*bottom_rpm_line_q+bottom_polynom_coef[0,2])-bottom_rpm_line_h))
    right_polynom_max_error = np.amax(np.absolute((right_polynom_coef[0,
                                      0]*np.square(right_eff_line_q) + right_polynom_coef[0,1]*right_eff_line_q+right_polynom_coef[0,2])-right_eff_line_h))
    left_polynom_max_error = np.amax(np.absolute(
        (left_polynom_coef[0,0]*np.square(left_eff_line_q) + left_polynom_coef[0,1]*left_eff_line_q+left_polynom_coef[0,2])-left_eff_line_h))
    maxfiterror = max(top_polynom_max_error, bottom_polynom_max_error, right_polynom_max_error, left_polynom_max_error)
    return maxfiterror

def _organize_wa_coef(bottom_polynom_coef, convexity):
    #convexity is nul if it is convex, i.e. it is in the bottom

    appended_coef = []
    colnumber=(np.shape(bottom_polynom_coef[0,:]))[0]

    for idx, line in enumerate(bottom_polynom_coef[:,0]):
        #if colnumber == 2:
        #    print('true')
        #    one_line = np.concatenate(([0],[bottom_polynom_coef[idx,1]], [0, -1], [bottom_polynom_coef[idx,0]], [0, convexity]))
        #else:
        if size(convexity) > 1:
            convexity = convexity[idx]
        one_line = np.concatenate(([bottom_polynom_coef[idx,0]], [bottom_polynom_coef[idx,1]], [0, -1], [bottom_polynom_coef[idx,2]], [0, convexity]))
        if idx == 0:
            appended_coef=np.reshape([one_line],(1,7))
        else:
            appended_coef=np.append(appended_coef,np.reshape([one_line],(1,7)), axis = 0)
    return appended_coef

def _working_area_calc(bottom_polynom_coef, top_polynom_coef, left_polynom_coef, right_polynom_coef,
                       point_coordinates, pump_type, extra_line_coef, convexity_parameter):
    print(_organize_wa_coef(extra_line_coef, convexity_parameter))
    #It gives back numbers in an order q2 q, h2, h, number
    if pump_type == 2:
        working_area_coef = extra_line_coef
    else:
        working_area_coef = np.concatenate((_organize_wa_coef(bottom_polynom_coef, 0),_organize_wa_coef(top_polynom_coef, 1),_organize_wa_coef(left_polynom_coef, 1),_organize_wa_coef(right_polynom_coef, 0), _organize_wa_coef(extra_line_coef, convexity_parameter)),axis=0)
    for line_num in range(working_area_coef.shape[0]):
        if abs(working_area_coef[line_num, 0]) < 1e-7 and abs(working_area_coef[line_num, 1]+1) < 1e-7:
            working_area_coef[line_num, 5] = 1
            #It is given in H

    # Giving the location related to the point
    #It would be better to use the point only for the extras

    for line_num in range(working_area_coef.shape[0]):
        #if pump_type == 0 or pump_type == 1:
        #    if line_num == 0:
        #        working_area_coef[line_num, 6] = 0 #bottom
        #    elif line_num == 1:
        #        working_area_coef[line_num, 6] = 1 # top
        #    elif line_num == 2:
        #        working_area_coef[line_num, 6] = 1 #left
        #    elif line_num == 3:
        #        working_area_coef[line_num, 6] = 0  #right

        if abs(working_area_coef[line_num, 5]-1) < 1e-7:
            print ('Given in H')
            logger.info('A boundary for the working area given in H')
            line_coord = working_area_coef[line_num, 2]*point_coordinates[1]**2 + \
                working_area_coef[line_num, 3]*point_coordinates[1]+working_area_coef[line_num, 4]
            if line_coord > point_coordinates[0]:
                working_area_coef[line_num, 6] = 1
        else:
            logger.info('A boundary for the working area given in Q')
            line_coord = working_area_coef[line_num, 0]*point_coordinates[0]**2 + \
                working_area_coef[line_num, 1]*point_coordinates[0]+working_area_coef[line_num, 4]
            if line_coord > point_coordinates[1]:
                working_area_coef[line_num, 6] = 1

    return working_area_coef

def _extra_area_calc(pump_type, extra_line_coef, point_coordinates):
    working_area_coef = extra_line_coef
    for line_num in range(working_area_coef.shape[0]):
        if abs(working_area_coef[line_num, 0]) < 1e-7 and abs(working_area_coef[line_num, 1]+1) < 1e-7:
            working_area_coef[line_num, 5] = 1
            #If Q has no coefficient, the line is given in H.
            #We set a 1 to the 5th column of this array to indicate that. Otherwise it is zero, indication given in Q

    # Giving the location related to the point

    for line_num in range(working_area_coef.shape[0]):

        if abs(working_area_coef[line_num, 5]-1) < 1e-7:
            logger.info('A boundary for the working area given in H')
            line_coord = working_area_coef[line_num, 2]*point_coordinates[1]**2 + \
                working_area_coef[line_num, 3]*point_coordinates[1]+working_area_coef[line_num, 4]
            #These calculate the Q coordinate of the line belonging to the H coord of the point (since given in Q)
            if line_coord > point_coordinates[0]:
                working_area_coef[line_num, 6] = 1
        else:
            logger.info('A boundary for the working area given in Q')
            line_coord = working_area_coef[line_num, 0]*point_coordinates[0]**2 + \
                working_area_coef[line_num, 1]*point_coordinates[0]+working_area_coef[line_num, 4]
            if line_coord > point_coordinates[1]: #upper boundary
                working_area_coef[line_num, 6] = 1

    return working_area_coef

def _2darray_to_np_flattened(H):
    row, col = shape(H)
    H4 = np.reshape(H, (row*col, 1))
    H5 = np.ravel(H4)
    return H5

if __name__ == '__main__':
    pass
