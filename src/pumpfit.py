#from typing import List
import numpy as np
import logging
import casadi as ca
from casadi import *


logger = logging.getLogger("pump-fitting-tools")
logging.basicConfig(level=logging.INFO)

class Pump:
    def __init__(self, Q, H, P, E,N, loss_q=0.0):
        #This function expects lists or arrays the same length, except for N, which should be an integer
        #It also expects increasing time series
        #assert (np.size(N)<2), "The pump speed should be a single number"
        if not (np.size(N)<2):
            raise Exception("The pump speed should be a single number")
        if type(N) is list:
            raise Exception("The pump speed should be a single number")
        if not N>0.0:
            raise Exception("The pump speed should be positive")
        if not np.size(H) == np.size(Q):
            raise Exception("Inputs values should be the same size")
        if min(Q) < 0.0:
            raise Exception("Discharge should be positive")
        #assert (np.size(H) == np.size(Q)), "Inputs values should be the same size"
        #assert (min(Q) >= 0), "Discharge should be positive"
        self.Q = Q
        self.H = H
        self.N = N

        sorted_array = np.argsort(H)
        #assert (np.invert(np.any(np.invert((sorted_array == range(len(H)-1,-1,-1)))))), "H values should be decreasing"
        if not np.invert(np.any(np.invert((sorted_array == range(len(H)-1,-1,-1))))):
            raise Exception("H values should be decreasing")

        sorted_array_q = np.argsort(Q)
        #assert (np.invert(np.any(np.invert((sorted_array_q == range(0,len(Q))))))), "Q values should be increasing"
        if not np.invert(np.any(np.invert((sorted_array_q == range(0,len(Q)))))):
            raise Exception("Q values should be increasing")

        #Either power or efficiency of both should be given
        #In the latter case they should be consistent, if they are not, warning is given and eff. is used
        if np.size(E)==0:
            print("Efficiency is calculated from power")
            if not np.size(H) == np.size(P):
                raise Exception("Inputs values should be the same size")
            if not min(P)>0:
                raise Exception("Power should be positive")
            E_calculated = np.zeros((len(H),1))
            for k in range(len(H)):
                E_calculated[k] = 1000*9.81*H[k]*Q[k]/P[k]
            self.E = E_calculated
            self.P = P

        elif np.size(P)==0:
            print("Power is calculated from efficiency")
            if not np.size(H) == np.size(E):
                raise Exception("Inputs values should be the same size")
            if not min(E)>0:
                raise Exception("Efficiency should be positive")

            P_calculated = np.zeros((len(Q),1))
            for k in range(len(H)):
                P_calculated[k] = 1000*9.81*H[k]*Q[k]/E[k]
                if P_calculated[k] == 0:
                    P_calculated[k] = 1000*9.81*H[k+1]*Q[k+1]/E[k+1] #This is not the best solution for zero Q
            self.P = P_calculated
            self.E = E

        else:
            #assert (np.size(H) == np.size(P)), "Inputs values should be the same size"
            #assert (np.size(H) == np.size(E)), "Inputs values should be the same size"
            if not np.size(H) == np.size(P):
                raise Exception("Input values should be the same size")
            if not np.size(H) == np.size(E):
                raise Exception("Input values should be the same size")
            E_calculated = 1000*9.81*H*Q/P
            efficiency_inconsistency = abs(E_calculated-E)
            #assert (max(efficiency_inconsistency) < 0.05), "Efficiency and power are inconsistent, give only one of them"
            if not max(efficiency_inconsistency) < 0.05:
                raise Exception("Efficiency and power are inconsistent, give only one of them")
            self.P = P
            self.E = E

        # self.losshref = 0.0
        # self.lossqref = loss_q
        self.minimum_efficiency = 0.4
        self.minimum_rpm = 0.4
        self.point_coordinates = [0.0, 0.0]
        self.ackeret_correction = 0
        self.extra_line_coef = np.zeros((1,7))
        self.pump_type = 0
        self.loss_coefficient = 0.0
        self.maximum_rpm = 1.0
        self.maximum_calc_rpm = 1.0
        self.constant_head = 0


    def some_method(my_arg, *args, **kwargs):
        some_other_method(*args, **kwargs)

    def some_other_method(q, h, *args, **kwargs):
        pass


    # def fit(n_lines, max_error, quadratic, *args, **kwargs):
    #     fit_power(*args, **kwargs)


    def fit_power(cutoff_efficiency=0.3, **kwargs):
        pass

    def fit_working_area():
        self._fit()

        return


    # def fit(self):
    #     {'power': self.fit_power(),
    #     ''
    #     pass
    #     }
    # }


    # p.fit(cutoff_efficiency=0.5)


    # some_method(1.0, 2.0, h=1.0, blabla='boe')

    # pump_type.SCREW_PUMP
    # PumpType.SCREW_PUMP
    # PumpType.NORMAL


    def _generating_points(self, number_generated_points=15, pump_type=0, ackeret_correction=0, minimum_rpm=0.5, maximum_rpm=1.0, maximum_calc_rpm=1.0):
        #This function generates points between the minimum and maximum (calc) rpm.It uses the affinity laws, according to which E does not change,
        # N is the given one, and H, Q and P change as given.
        Q = self.Q
        H = self.H
        P = self.P
        E = self.E
        N = self.N
        #The given efficiency is only used if there is no Ackeret correction.
        NlistProp = np.linspace(minimum_rpm, maximum_calc_rpm, number_generated_points)
        H3 = np.zeros((len(H), number_generated_points))
        Q3 = np.zeros((len(H), number_generated_points))
        P3 = np.zeros((len(H), number_generated_points))
        E3 = np.zeros((len(H), number_generated_points))
        N3 = np.zeros((len(H), number_generated_points))

        for i in range(0, len(H)):
            for ii in range(0, number_generated_points):
                Nnew = NlistProp[ii]*N / maximum_rpm
                if pump_type == 2:
                   Q3[i, ii] = Q[i]*Nnew/N
                   H3[i, ii] = H[i]
                   P3[i, ii] = P[i]*(Nnew/N)
                else:
                   Q3[i, ii] = Q[i]*Nnew/N
                   H3[i, ii] = H[i]*(Nnew/N)**2
                   P3[i, ii] = P[i]*(Nnew/N)**3
                E3[i, ii] = E[i]
                N3[i, ii] = Nnew

                if ackeret_correction == 1:
                    #Here we recalculate the efficiency so that it is the one that corresponds to the H
                    #that already includes losses. This might only be necessary if losses added as extra.
                    E3[i, ii] = 1000 * 9.81 * H3[i,ii] * Q3[i, ii]/(P3[i, ii])
                    alpha = 0.15
                    corrected_efficiency = 1.0 - (1.0-E3[i, ii])*pow((N/Nnew), alpha)
                    if corrected_efficiency < -1.0:
                        E3[i, ii] = -1.0
                    elif corrected_efficiency > 1.0:
                        E3[i, ii] = 1.0
                    else:
                        E3[i, ii] = corrected_efficiency
                    # Recalculating the power with the corrected efficiency, in this way the Ackeret correction
                    # is inlcuded in the power calculation
                    P3[i, ii] = 1000 * 9.81 * H3[i, ii]* Q3[i, ii]/(E3[i, ii] )

        return H3, Q3, E3, N3, P3

    def _removing_losses(self):
        Hman = self.H
        Q = self.Q
        losshref = self.losshref
        lossqref = self.lossqref
        loss_coefficient = self.loss_coefficient
        E = self.E
        P = self.P
        #This function removes the head losses, that is calculated the static head
        #by using the equation Hstat = Hman - H losses
        #H losses can be added in two ways: either with a loss coefficient or
        # a known loss for a known Q
        #In the end of the file the efficiency is calculated. It is needed because for the
        #power approximation we need static efficiency, and maybe it was manometric before
        Hstat = np.empty_like(Hman)
        if lossqref < 1e-7:
            Hstat = Hman
        else:
            for k, h in enumerate(Hman):
                Hstat[k] = Hman [k] - losshref*(Q[k]/lossqref)**2 - loss_coefficient * Q[k]**2
                if Hstat[k] < 0:
                    logger.error('Too much losses, H became negative')
                E[k] = 1000 * 9.81 * Hstat[k] * Q[k] / P[k]
        self.H = Hstat
        self.E = E
        return Hstat, E


    def _restrict_points_efficiency(self, H3, Q3, E3, P3, N3, minimum_efficiency):
        # Creating the simpl ones again
        H = []
        Q = []
        P = []
        E = []
        N = []
        left_eff_line_h = []
        left_eff_line_q = []
        right_eff_line_h = []
        right_eff_line_q = []

        if E3[0,0] < minimum_efficiency:
            start_switch = 0
        else:
            start_switch = 1
            left_eff_line_h = H3[0, ]
            left_eff_line_q = Q3[0, ]
            start_index = 0

        if E3[-1,-1] < minimum_efficiency:
            end_switch = 0
        else:
            end_switch = 1
            right_eff_line_h = H3[-1, ]
            right_eff_line_q = Q3[-1, ]
            finish_index = np.shape(H3)[0]

        # This block selects the points that are relevant for the working area
        ii = 1
        for i in range(0, np.shape(H3)[0]):
            if start_switch == 0 and E3[i, ii] >= minimum_efficiency:
                start_switch = 1
                left_eff_line_h = (H3[i, ])
                left_eff_line_q = (Q3[i, ])
                start_index = i
            if (end_switch == 0 and start_switch == 1) and (E3[i, ii] < minimum_efficiency and i > 0):
                end_switch = 1
                finish_index = i-1
                right_eff_line_h = (H3[i-1, ])
                right_eff_line_q = (Q3[i-1, ])

        #--------------------------------------------------------------

        for i in range(0, np.shape(H3)[0]):
            for ii in range(0, np.shape(H3)[1]):
                if i >= start_index and i <= finish_index:
                    H.append(H3[i, ii])
                    Q.append(Q3[i, ii])
                    P.append(P3[i, ii])
                    E.append(E3[i, ii])
                    N.append(N3[i, ii])


        # Using the indices found
        bottom_rpm_line_h = H3[start_index:finish_index+1, 0]
        bottom_rpm_line_q = Q3[start_index:finish_index+1, 0]

        top_rpm_line_h = H3[start_index:finish_index+1, -1]
        top_rpm_line_q = Q3[start_index:finish_index+1, -1]


        H2 = np.reshape(np.array(H),(1 ,np.size(H)))
        Q2 = np.reshape(np.array(Q),(1 ,np.size(Q)))
        P2 = np.reshape(np.array(P),(1 ,np.size(P)))
        E2 = np.reshape(np.array(E),(1 ,np.size(E)))
        N2 = np.reshape(np.array(N),(1 ,np.size(N)))

        restricted_area = {'H':H2, 'Q':Q2, 'P':P2, 'E':E2,'N':N2, 'bottom_rpm_line_h':bottom_rpm_line_h, 'bottom_rpm_line_q':bottom_rpm_line_q, 'top_rpm_line_h': top_rpm_line_h, 'top_rpm_line_q': top_rpm_line_q, 'left_eff_line_h':left_eff_line_h, 'left_eff_line_q': left_eff_line_q, 'right_eff_line_h':right_eff_line_h, 'right_eff_line_q': right_eff_line_q}
        return restricted_area

    def _restrict_points_for_working_area(self, H3, Q3, P3, E3, N3, working_area_coef, maxfiterror=1e-2, pump_type=0):
        #This part creates the restricted points for the third time
        #The data about the array is stored in the [5] and [6] entry
        #if [5] is 1 is H boundary, if it is 0, then Q
        #if [6] is 1 is top boundary, it is 0, bottom boundary
        append_array_index=np.ones_like(H3)
        diff_q_tolerance=0.25
        diff_h_tolerance=maxfiterror
        diff_h_tolerance=0.25

        if pump_type == 1:
            diff_q_tolerance=0.5
            diff_h_tolerance=0.5

        for i in range(0, np.shape(H3)[0]):
            for ii in range(0, np.shape(H3)[1]):
                append_it=1
                h_coord=H3[i,ii]
                q_coord=Q3[i,ii]
                for row in range(working_area_coef.shape[0]):
                    if abs(working_area_coef[row, 5])<1e-7:
                        #we have q, we evaluate the polynom plugging in q
                        poly_h_value=working_area_coef[row,0]*q_coord**2+working_area_coef[row, 1]*q_coord+working_area_coef[row,4]
                        if abs(working_area_coef[row, 6])<1e-7:
                            #bottom boundary, point coordinate should be above it (disqualified if it is below)
                            if h_coord < poly_h_value-diff_h_tolerance:
                                append_it=0
                        else:
                            #lower boundary, the point coordinate should be under it, if not, gets a "no" for joining
                            if h_coord > poly_h_value + diff_h_tolerance:
                                append_it=0
                    else:
                        #we have h, we evaluate the polynom plugging in h
                        poly_q_value=working_area_coef[row,2]*h_coord**2+working_area_coef[row, 3]*h_coord+working_area_coef[row,4]
                        if abs(working_area_coef[row, 6])<1e-7:
                            #lower boundary
                            if q_coord < poly_q_value - diff_q_tolerance:
                                append_it=0
                        else: #upper boudnary
                            if q_coord > poly_q_value + diff_q_tolerance:
                                append_it=0
                append_array_index[i,ii]=append_it

        Q=[]
        H=[]
        E=[]
        P=[]
        N=[]

        for i in range(0, np.shape(H3)[0]):
            for ii in range(0, np.shape(H3)[1]):
                if append_array_index[i,ii]==1:
                    H.append(H3[i, ii])
                    Q.append(Q3[i, ii])
                    P.append(P3[i, ii])
                    E.append(E3[i, ii])
                    N.append(N3[i, ii])

        number_of_points = len(H)
        P_=np.ravel(np.reshape(P,(number_of_points, 1)))
        E_=np.ravel(np.reshape(E,(number_of_points, 1)))
        Q_=np.ravel(np.reshape(Q,(number_of_points, 1)))
        H_=np.ravel(np.reshape(H,(number_of_points, 1)))
        N_=np.ravel(np.reshape(N,(number_of_points, 1)))

        return Q_, H_, E_, P_, N_



    def _extra_area_calc(self):
        pump_type = self.pump_type
        extra_line_coef = self.extra_line_coef
        point_coordinates = self.point_coordinates
        working_area_coef = extra_line_coef

        for line_num in range(working_area_coef.shape[0]):
            if abs(working_area_coef[line_num, 0]) < 1e-7 and abs(working_area_coef[line_num, 1]+1) < 1e-7:
                working_area_coef[line_num, 5] = 1
                #If Q has no coefficient, the line is given in H.
                #We set a 1 to the 5th column of this array to indicate that. Otherwise it is zero, indication given in Q

        # Giving the location related to the point

        for line_num in range(working_area_coef.shape[0]):

            if abs(working_area_coef[line_num, 5]-1) < 1e-7:
                logger.info('A boundary for the working area given in H')
                line_coord = working_area_coef[line_num, 2]*point_coordinates[1]**2 + \
                    working_area_coef[line_num, 3]*point_coordinates[1]+working_area_coef[line_num, 4]
                #These calculate the Q coordinate of the line belonging to the H coord of the point (since given in Q)
                if line_coord > point_coordinates[0]:
                    working_area_coef[line_num, 6] = 1
            else:
                logger.info('A boundary for the working area given in Q')
                line_coord = working_area_coef[line_num, 0]*point_coordinates[0]**2 + \
                    working_area_coef[line_num, 1]*point_coordinates[0]+working_area_coef[line_num, 4]
                if line_coord > point_coordinates[1]: #upper boundary
                    working_area_coef[line_num, 6] = 1

        return working_area_coef

    @classmethod
    def _approximating_polynoms(self, H, Q, convexity_parameter, constant_speed=0):
        #This function gives back the coefficients of a second order convex polynom
        #It is expressed in Q
        # conveiity is 1 if convex and 0 if concave
        #First it makes a fit, and then it checks if it is convex.
        #If not, it makes a linear fit.

        coef = ca.SX.sym('coef', 3)
        QQ = ca.SX.sym('QQ')

        # Create symbolic funciton for the power
        if constant_speed == 0 or constant_speed == 2:
            qh_curve = coef[0]*QQ**2+coef[1]*QQ+coef[2]
        elif constant_speed == 1:
            qh_curve = coef[1]*QQ+coef[2]
        else:
            logger.warning("Constant speed chosing number must be 1, 2 or 0")
            qh_curve = coef[0]*QQ**2+coef[1]*QQ+coef[2]

        qh_curve_symbolic = ca.Function('qh_curve_symbolic', [QQ, coef], [qh_curve])

        # Creating the objective function
        objective_function = SX.sym('objective_function')
        objective_function = 0

        for k in range(0, len(H)):
            objective_function = objective_function + ( qh_curve_symbolic(Q[k], coef)[0] -H[k] ) **2

        nlp = {'f': objective_function, 'g': [], 'x': coef}
        options = {'print_time': False, 'ipopt': {'print_level': 0}}
        solver = ca.nlpsol('nlp', 'ipopt', nlp, options)

        results = solver(ubg = 0)
        solution = np.array(results['x']).flatten()

        objective_value = np.array(results['f']).flatten()

        #Here is checked if the solution is convex. If not, we make a linear fit
        if solution[0]*convexity_parameter < 0: #or solution[1]*convexity_parameter < 0:
            print("First attempt was not convex")
            qh_curve = coef[1]*QQ+coef[2]
            qh_curve_symbolic = ca.Function('qh_curve_symbolic', [QQ, coef], [qh_curve])

            # Creating the objective function
            objective_function = SX.sym('objective_function')
            objective_function = 0

            for k in range(0, len(H)):
                objective_function = objective_function + ( qh_curve_symbolic(Q[k], coef)[0] -H[k] ) **2

            nlp = {'f': objective_function, 'x': coef}
            options = {'print_time': False, 'ipopt': {'print_level': 0}}
            solver = ca.nlpsol('nlp', 'ipopt', nlp, options)
            results = solver()

            solution = np.array(results['x']).flatten()

        polynom_coef = [float(solution[0]), float(solution[1]), float(solution[2])]
        polynom_coef = numpy.reshape( numpy.asarray(polynom_coef   ), (1,np.shape(numpy.asarray(polynom_coef))[0]))
        return polynom_coef, convexity_parameter

    def _working_area_calc(self, bottom_polynom_coef, top_polynom_coef, left_polynom_coef, right_polynom_coef,
                       point_coordinates, pump_type, extra_line_coef, convexity_parameter):
        #It gives back numbers in an order q2 q, h2, h, number
        if pump_type == 2:
            working_area_coef = extra_line_coef
        else:
            working_area_coef = np.concatenate((self._organize_wa_coef(bottom_polynom_coef, 0),self._organize_wa_coef(top_polynom_coef, 1),self._organize_wa_coef(left_polynom_coef, 1),self._organize_wa_coef(right_polynom_coef, 0), self._organize_wa_coef(extra_line_coef, convexity_parameter)),axis=0)
        for line_num in range(working_area_coef.shape[0]):
            if abs(working_area_coef[line_num, 0]) < 1e-7 and abs(working_area_coef[line_num, 1]+1) < 1e-7:
                working_area_coef[line_num, 5] = 1
                #It is given in H

        # Giving the location related to the point
        #It would be better to use the point only for the extras

        for line_num in range(working_area_coef.shape[0]):
            if abs(working_area_coef[line_num, 5]-1) < 1e-7:
                print ('Given in H')
                logger.info('A boundary for the working area given in H')
                line_coord = working_area_coef[line_num, 2]*point_coordinates[1]**2 + \
                    working_area_coef[line_num, 3]*point_coordinates[1]+working_area_coef[line_num, 4]
                if line_coord > point_coordinates[0]:
                    working_area_coef[line_num, 6] = 1
            else:
                logger.info('A boundary for the working area given in Q')
                line_coord = working_area_coef[line_num, 0]*point_coordinates[0]**2 + \
                    working_area_coef[line_num, 1]*point_coordinates[0]+working_area_coef[line_num, 4]
                if line_coord > point_coordinates[1]:
                    working_area_coef[line_num, 6] = 1

        return working_area_coef

    def _approximate_convex_extra_lines(self,extra_line_coef, Q):

        print(extra_line_coef)
        cshape = extra_line_coef.shape
        row=cshape[0]
        convex_polynom_coef_array= np.zeros((row,3))
        convexity_parameter_array=np.zeros((row,1))
        q_points = np.linspace(min(Q),max(Q),20)
        for k in range(row):
            h_points = np.square(q_points)*extra_line_coef[k,1]+q_points * extra_line_coef[k,1] + extra_line_coef[k,4]
            convex_polynom_coef, convexity_parameter=  self._approximating_polynoms(h_points, q_points, -1, 0)
            convexity_parameter_array [k] = convexity_parameter
            convex_polynom_coef_array[k,:] = convexity_parameter
        print(convex_polynom_coef_array)
        return convex_polynom_coef_array, convexity_parameter_array



    def _organize_wa_coef(self,bottom_polynom_coef, convexity):
        #convexity is nul if it is convex, i.e. it is in the bottom

        appended_coef = []
        colnumber=(np.shape(bottom_polynom_coef[0,:]))[0]

        for idx, line in enumerate(bottom_polynom_coef[:,0]):
            if np.size(convexity) > 1:
                convexity = convexity[idx]
            one_line = np.concatenate(([bottom_polynom_coef[idx,0]], [bottom_polynom_coef[idx,1]], [0, -1], [bottom_polynom_coef[idx,2]], [0, convexity]))
            if idx == 0:
                appended_coef=np.reshape([one_line],(1,7))
            else:
                appended_coef=np.append(appended_coef,np.reshape([one_line],(1,7)), axis = 0)
        return appended_coef

    def _approximate_linear_working_area_boundaries(self, restricted_area, point_coordinates,extra_line_coef, Q4, pump_type):
        bottom_polynom_coef, convexity_parameter = self._approximating_polynoms(restricted_area['bottom_rpm_line_h'], restricted_area['bottom_rpm_line_q'], 1, constant_speed=0)
        top_polynom_coef, convexity_parameter = self._approximating_polynoms(restricted_area['top_rpm_line_h'], restricted_area['top_rpm_line_q'], -1, constant_speed=0)
        right_polynom_coef, convexity_parameter = self._approximating_polynoms(restricted_area['right_eff_line_h'], restricted_area['right_eff_line_q'], 1, constant_speed=0)
        left_polynom_coef, convexity_parameter = self._approximating_polynoms(restricted_area['left_eff_line_h'], restricted_area['left_eff_line_q'], -1, constant_speed=0)
        convex_polynom_coef_array, convexity_parameter_array = self._approximate_convex_extra_lines(extra_line_coef, Q4)
        working_area_coef = self._working_area_calc(bottom_polynom_coef, top_polynom_coef, left_polynom_coef, right_polynom_coef,
                                           point_coordinates, pump_type, convex_polynom_coef_array, convexity_parameter_array)

    # It is the opposite, it starts with the square
    #def __init__(self, Q, H, Eff, Power, Nref, loss_h):
    #    self.Q = Q_bnd

    #def fit_working_area(self, ...) -> List[np.ndarray]:
    #    return ...

    #def fit_power(self):
    #    self._do_something_that_is_shared_between()
    #    return ...

    #@staticmethod
    #def _do_something_that_is_shared_between(self, ):
    #    self.Q
    #    pass
    #def plot_()