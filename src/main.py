'''
This code is reading a csv file with and fits working area of a pump
To do:
         - test it with different pumps
         - comment more
         - what happens with the using 1 point instead of 2
         - even for constant speed pump the minimum efficiecy should be valid
'''

# Make an 1 extry entries to the array, 0 if the good number is over it (beneden grenz), make it for q and h
# Note that the upper or lower bondary is always relative to the variable it is expressed
# Substitute the point 's coordinate, if the other coordinate is smaller then the result then the polynom is a boven grenz 1
# Choosing the right efficiency points
# For the left and right all points are used
# For the bottom and the top, points should be selected


from sys import path
#path.append("D:\Casadi")
#! =====================
from casadi import *
from numpy import *
import scipy
import csv
import sys
import logging
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from pump_plotting import _plotting_gradient, _plotting_power, _plotting_level_curve_surface, _plotting_working_area_boundary, _plotting_working_area, _write_working_area, _write_approximation
from pump_plotting import _csv_reader, _plotting_points, _level_curves, _plotting_points2, _plotting_curves, _plotting_losses, _write_power, _plotting_working_area_boundary2
from pump_fitting import _approximating_power, _working_area_calc, _approximating_speed, _approximating_power_linear
from pump_fitting import _approximating_polynoms, _approximating_polynoms_linear
from pump_fitting import _generating_points, _max_fit_error, _return_angle
from pump_fitting import _restrict_points_efficiency, _approximating_wa_linear
from pump_fitting import _restrict_points_for_woring_area, _removing_losses, _boundary_point_detection2
from pump_fitting import _approximating_polynoms_top, _extra_area_calc, _approximate_convex_extra_lines
logger = logging.getLogger("pump-fitting-tools")
logging.basicConfig(level=logging.INFO)

if __name__ == '__main__':

    csv_file_name = 'Kuijkm_input_extrap'
    csv_file_name = 'Pannerling_input_extrap'
    #csv_file_name = 'Pannerling_input'
    #csv_file_name = 'Kuijkm_input'
    #csv_file_name = 'ExampleHomePage'
    #csv_file_name = 'Beuningen1_input_extrap'  #This is a constant speed pump
    #csv_file_name = 'buma1'

    linear = 0

    #This function reads the variables from the csv
    P, E, Q, Hread, dref, N, losshref, lossqref, minimum_efficiency, minimum_rpm, point_coordinates, ackeret_correction, extra_line_coef, pump_type, loss_coefficient, maximum_rpm, maximum_calc_rpm, constant_head = _csv_reader(csv_file_name)

    #This function removes the losses if they are expressed in Q and H
    H, E = _removing_losses(Hread, Q, losshref, lossqref, loss_coefficient, E, P)

    #This line plots the losses
    #_plotting_losses(Q, Hread, H)

    # It generates points using the affinity laws
    # It is also possible to use the Ackeret correction
    # Creating arrays with the number 3
    if pump_type == 1:
        number_generated_points=2
    else:
        number_generated_points=15
    H3, Q3, E3, N3, P3 = _generating_points(H, Q, E, N, P, number_generated_points, pump_type, ackeret_correction, minimum_rpm, maximum_rpm, maximum_calc_rpm)
    print(type(H3))
    print(shape(H3))
    print(type(H))
    print(shape(H))
    #Check what these do, they are only for screw pumps
    if pump_type == 2:
        _plotting_points(H3, Q3, E3, 'EffVijzel')
        _plotting_points2(H3, Q3, P3, 'PowerVijzel')
        _level_curves(H3, Q3, P3, 'LevelCurvesPower')
        _level_curves(H3, Q3, E3, 'LevelCurvesEfficiency')
        _level_curves(H3, Q3, N3, 'LevelCurvesSpeed')

    maxfiterror = 1e-2

    #This function restricts the points based on gien power and efficiency
    restriced_area= _restrict_points_efficiency(H3, Q3, E3, P3, N3, minimum_efficiency)
    Q4 = restriced_area['Q']
    P4 = restriced_area['P']
    H4 = restriced_area['H']
    E4 = restriced_area['E']
    N4 = restriced_area['N']
    bottom_rpm_line_h = restriced_area['bottom_rpm_line_h']
    bottom_rpm_line_q = restriced_area['bottom_rpm_line_q']
    top_rpm_line_h = restriced_area['top_rpm_line_h']
    top_rpm_line_q = restriced_area['top_rpm_line_q']
    left_eff_line_h = restriced_area['left_eff_line_h']
    left_eff_line_q = restriced_area['left_eff_line_q']
    right_eff_line_h = restriced_area['right_eff_line_h']
    right_eff_line_q = restriced_area['right_eff_line_q']

    # This part adds the extra lines, uses the point to determine which kind of boundaries they are
    extra_line_coef_calc =  _extra_area_calc(pump_type, extra_line_coef, point_coordinates)

    H5 = np.reshape(np.array(H4),(1 ,size(H4)))
    Q5 = np.reshape(np.array(Q4),(1 ,size(Q4)))
    P5 = np.reshape(np.array(P4),(1 ,size(P4)))
    E5 = np.reshape(np.array(E4),(1 ,size(E4)))
    N5 = np.reshape(np.array(N4),(1 ,size(N4)))

    #This function restricts the points using the extra lines
    Q, H, E, P, N = _restrict_points_for_woring_area(H5, Q5, P5, E5, N5, extra_line_coef_calc, maxfiterror, pump_type)
    _plotting_working_area_boundary(Q, H, E, extra_line_coef, point_coordinates, Q3, H3)
    # At this point nothing is approximated


    #This part gives the points that are inside the working area. They power will be approximated based on them
    #The next part is completely apart from the power calculation
    #Until this point there is no difference between linear and non-linear behaviour
    #-------------------------------------------------------------------------------------------
    # This part calculates the working_area_coef array, the coefficients of the boudnary of the working area

    if linear == 1:
        #This part crates the functions approximating the boundary of the working area
        boundary_point_label, Q_bnd, H_bnd = _boundary_point_detection2(Q, H)
        new_working_area_coef = _approximating_wa_linear(H_bnd, Q_bnd)
        working_area_coef =  _extra_area_calc(pump_type, new_working_area_coef, point_coordinates)

    else:
        bottom_polynom_coef, convexity_parameter = _approximating_polynoms(bottom_rpm_line_h, bottom_rpm_line_q, 1, pump_type)
        top_polynom_coef = _approximating_polynoms_top(top_rpm_line_h, top_rpm_line_q, -1, pump_type)
        right_polynom_coef, convexity_parameter = _approximating_polynoms(right_eff_line_h, right_eff_line_q, 1, pump_type)
        left_polynom_coef, convexity_parameter = _approximating_polynoms(left_eff_line_h, left_eff_line_q, -1, pump_type)
        convex_polynom_coef_array, convexity_parameter_array = _approximate_convex_extra_lines(extra_line_coef_calc, Q)
        working_area_coef = _working_area_calc(bottom_polynom_coef, top_polynom_coef, left_polynom_coef, right_polynom_coef,
                                           point_coordinates, pump_type, convex_polynom_coef_array, convexity_parameter_array)
    #Plot and write the calculated coefficients
    if linear ==1:
        _plotting_working_area_boundary2(Q, H, Q_bnd, H_bnd, working_area_coef, point_coordinates)
    else:
        _plotting_working_area(Q3, H3, E3,
                           bottom_rpm_line_q, bottom_rpm_line_h, top_rpm_line_q, top_rpm_line_h, left_eff_line_q,
                           left_eff_line_h, right_eff_line_q, right_eff_line_h,
                           bottom_polynom_coef, top_polynom_coef, left_polynom_coef, right_polynom_coef, working_area_coef, Q, H, E, point_coordinates,  'EfficiencyWorkingArea')
        _plotting_working_area_boundary2(Q, H, Q, H, working_area_coef, point_coordinates)


    _write_working_area(working_area_coef)
    #-------------------------------------------------------------------------------------------------------------------------------------------------------
    # Approximating rpm and power
    # This part is the approximation and it uses the points restricted based on the given curves

    # Note: for this optimisation the constraints are not convex
    # Therefore first we do an unconstrainted optimisation (that is convex)
    # and then we check convexity. If it is not convex, we run an optimisation
    # with constraints. However, this one might not find the right solution,
    # check always the results, especially if constrained optimisation is run

    print ("rpm")
    solution_rpm, rpm_calculated, rpm_error_calculated = _approximating_speed(H, Q, N, 0, 'rpm')

    print ("effficiency")
    solution_eff, eff_calculated, eff_error_calculated = _approximating_power(H, Q, E, 0, 'effficiency')

    print ("power")
    if pump_type == 1:
        if linear ==1:
            solution, power_calculated, error_calculated = _approximating_power_linear(H, Q, P, 1, 'power',constant_head)
        else:
            solution, power_calculated, error_calculated = _approximating_power(H, Q, P, 1, 'power',constant_head)
    else:
        if linear == 1:
            solution, power_calculated, error_calculated = _approximating_power_linear(H, Q, P, 1, 'power',constant_head)
        else:
            solution, power_calculated, error_calculated = _approximating_power(H, Q, P, 0, 'power',constant_head)
            if (solution[0]+solution[2]) < 0 or 4*(solution[0]*solution[2])-solution[4]**2 < 0:
                print ("Power: Constrained optimisation is run (non-convex)")
                solution, power_calculated, error_calculated = _approximating_power(H, Q, P, 1, 'power',constant_head)

    # Plotting
    if pump_type != 1:
        _plotting_gradient(H, Q, P, power_calculated)
        _plotting_power(H, Q, P, power_calculated, error_calculated)
        #_plotting_power(H, Q, P, error_calculated)
        # Here the shape of the file is (3,). It is a list, but it can also be np array but with the same shape
        # If the array is two dimensional you can use the function ravel to make it really one dimensional
    _plotting_level_curve_surface(H, Q, P, power_calculated)
    #_plotting_working_area_boundary(Q, H, E, working_area_coef, point_coordinates, Q3, H3)
    #_plotting_working_area(Q3, H3, E3,
    #                       bottom_rpm_line_q, bottom_rpm_line_h, top_rpm_line_q, top_rpm_line_h, left_eff_line_q,
    #                       left_eff_line_h, right_eff_line_q, right_eff_line_h,
    #                       bottom_polynom_coef, top_polynom_coef, left_polynom_coef, right_polynom_coef, 'EfficiencyWorkingArea')

    if linear == 1:
        _write_power(solution)
    else:
        _write_approximation(solution, 'power')
    _write_approximation(solution_rpm, 'speed')
    _write_approximation(solution_eff, 'efficiency')
